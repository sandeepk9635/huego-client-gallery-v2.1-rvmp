import React, { Component } from 'react';
// import '../src/assets/scss/global.scss';

import BgWrapper from '../../../src/views/components/global/bgWrapper';
import InfoWrapper from '../../../src/views/components/global/infoWrapper';

import _ from 'lodash';

class Login extends Component {
	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
	};
	render() {
		return (
			<div className="wrapper">
				<div className="container f-left">
					<BgWrapper
						bgImage={
							this.props.projectData['coverImage'][
								'coverImageUrl'
							]
						}
					/>
					<InfoWrapper
						title={this.props.projectData['title']}
						screenName={'login'}
						{...this.props}
					/>
				</div>
			</div>
		);
	}
}

export default Login;
