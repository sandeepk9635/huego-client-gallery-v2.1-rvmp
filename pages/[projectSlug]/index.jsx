import React, { Component } from 'react';

import Header from '../../src/views/components/header/header';

import BgWrapper from '../../src/views/components/global/bgWrapper';
import InfoWrapper from '../../src/views/components/global/infoWrapper';
import AlbumThumbnailsWrapper from '../../src/views/components/uielements/albumThumbnailsWrapper';
import RightModalLayout from '../../src/views/components/uicomponents/modals/layouts/rightModal';

import Collections from '../../src/views/components/uicomponents/collections.jsx';

import ProjectController from '../../src/controllers/project';

import _ from 'lodash';
import Modal from '../../src/views/components/uicomponents/modals/index';

import { withRouter } from 'next/router';

import Slider from 'react-slick';

class Home extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			projectDetails: { albums: [] },
			signedUrl: {},

			isGetSignedURL: true,
			isLoading: true,
			activeAlbumKey: -1,
			isSetAlbumKey: false,
			settings: {
				dots: false,
				infinite: false,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
			},
			galleryImages: {},
			albumDetails: {},
			isGalleryLoading: true,

			isDisplayFavourites: false,

			screenName: 'projectHome',

			windowWidth: 0,
		};
		this.slider = React.createRef();

		this.albumScreen = React.createRef();
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
		this.getProjectDetails();
		this.getSignedURLForProject();

		this.validateUser();
		this.setState({
			windowWidth: window.innerWidth,
		});
	};

	goToAlbum = (albumKey) => {
		let album = this.state.projectDetails.albums[albumKey];
		this.props.router.push(`/${this.props.projectData.slug}/${album.slug}`);
	};

	renderSlickSlider = () => {
		const settings = {
			...this.state.settings,
		};
		if (
			this.state.isGetSignedURL === true ||
			this.state.isLoading === true
		) {
			null;
		} else {
			return (
				<div className={'pages-slick-slider-wrapper w-100p'}>
					<Slider
						{...settings}
						ref={(slider) => (this.slider = slider)}
					>
						<div></div>
					</Slider>
				</div>
			);
		}
	};

	toggleFavourites = () => {
		this.setState({
			isDisplayFavourites: !this.state.isDisplayFavourites,
		});
	};
	render() {
		return (
			<div className="wrapper">
				<div className={`container `} ref={this.container}>
					<Header
						screenName={this.state.screenName}
						onClick={(screenName) => this.goToScreen(screenName)}
						title={this.state.albumDetails.title}
						toggleFavourites={() => this.toggleFavourites()}
					/>
					<div className="page-wrapper">
						<div className="bg-info-wrapper">
							<BgWrapper
								bgImage={
									this.props.projectData['coverImage'][
										'coverImageUrl'
									]
								}
							/>
							<InfoWrapper
								title={this.props.projectData['title']}
								albumsCount={_.size(
									_.filter(this.state.projectDetails.albums, {
										isPublished: true,
									})
								)}
								screenName={this.state.screenName}
								onClickRouteToShare={() =>
									this.props.router.push(
										`/${this.props.query.projectSlug}/share`
									)
								}
								{...this.props}
							/>
							<div className="gradient-thumbs"></div>
						</div>
						{_.size(this.state.projectDetails.albums === 0) ? (
							<p>ALbums Empty State</p>
						) : this.state.isGetSignedURL === true ||
						  this.state.isLoading === true ? (
							<p>Loading...</p>
						) : (
							<AlbumThumbnailsWrapper
								albums={this.state.projectDetails.albums}
								signedUrl={this.state.signedUrl}
								onClick={(key) => this.goToAlbum(key)}
								isActive={this.state.activeAlbumKey}
								windowWidth={this.state.windowWidth}
							/>
						)}
					</div>

					<Modal
						handleClose={() => this.toggleFavourites()}
						show={this.state.isDisplayFavourites}
						modalType={'right'}
					>
						<RightModalLayout
							onClose={() => this.toggleFavourites()}
						>
							<Collections
								collectionType={'view'}
								projectSlug={this.props.query.projectSlug}
							/>
						</RightModalLayout>
					</Modal>
				</div>
			</div>
		);
	}
}

export default withRouter(Home);
