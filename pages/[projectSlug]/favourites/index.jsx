import { createRouter, Router } from 'next/router';
import React, { Component } from 'react';
import { withRouter } from 'next/router';

class FavouritesIndex extends Component {
	componentDidMount = () => {
		this.props.router.replace(`/${this.props.query.projectSlug}`);
	};
	render() {
		return null;
	}
}

export default withRouter(FavouritesIndex);
