import React, { Component } from 'react';

import Header from '../../../../src/views/components/header/header';

import Fav from '../../../../src/assets/svg/fav.svg';

import Slider from 'react-slick';

import ProjectController from '../../../../src/controllers/project';

import _ from 'lodash';
import { withRouter } from 'next/router';
import Loader from '../../../../src/views/components/global/loader';

import Modal from '../../../../src/views/components/uicomponents/modals/index';

import RightModalLayout from '../../../../src/views/components/uicomponents/modals/layouts/rightModal';

import Collections from '../../../../src/views/components/uicomponents/collections.jsx';

class FavouritesFullImage extends ProjectController {
	constructor() {
		super();
		this.state = {
			settings: {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				lazyload: true,
			},

			thumbSettings: {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 10, // make this dynamic, according to screen  size
				slidesToScroll: 1,
				arrows: false,
			},
			showActionbar: false,

			isGetSignedURL: true,
			isLoading: true,

			collectionImages: {},
			collections: {},

			imageDetails: {},
			isCollectionLoading: true,

			fullImageDownloaded: [],
			loadingImageDownloaded: [],

			isDisplayFavourites: false,

			slidesToScrollThumbs: 0,

			activeSlide: 0,

			shouldDisplayGrid: false,
			isMovedImage: false,
			windowWidth: 0,
			imagesStatus: {},
		};
		this.slider = React.createRef();
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		this.validateUser();

		this.getSignedURLForProject();
		this.getCollections();
		this.getCollectionImages(this.props.query.favouritesID);

		this.setState({
			slidesToScrollThumbs: window.innerWidth / 40,
		});
	};

	componentDidUpdate = () => {
		if (
			!this.state.isCollectionLoading &&
			!this.state.isGetSignedURL &&
			!this.state.isLoading &&
			_.size(this.state.imageDetails) === 0
		) {
			if (_.size(this.state.collectionImages) === 0) {
				this.props.router.replace(
					`/${this.props.query.projectSlug}/favourites/${this.props.query.favouritesID}`
				);
			} else {
				let images = this.state.collectionImages;

				let index = _.findIndex(images, {
					image_id: this.props.query.imageID,
				});

				this.setState({
					initialSlide: index,
				});

				if (this.state.isMovedImage) {
					this.setState({
						isMovedImage: false,
					});

					if (index === -1) {
						index =
							_.size(this.state.collectionImages) >
							this.state.currentImage
								? this.state.currentImage
								: 0;
						this.goToSlick(index);
						this.beforeSlideChange(index);
					} else {
						this.beforeSlideChange(index);
					}
				} else {
					this.beforeSlideChange(index);
				}
			}
		}
	};

	toggleFavourites = () => {
		this.setState({
			isDisplayFavourites: !this.state.isDisplayFavourites,
		});

		if (this.state.isDisplayFavourites === true) {
			this.setState({
				isLoading: true,
				isMovedImage: true,
				imageDetails: {},
			});
			this.getCollectionImages(this.props.query.favouritesID);
		}
	};

	goToSlick = (k) => {
		this.slider.slickGoTo(k);
	};

	toggleActionbar = () => {
		document.body.style.overflow = 'hidden';
		this.setState({
			showActionbar: !this.state.showActionbar,
		});
	};

	beforeSlideChange = (imageKey) => {
		let imageDetails = this.state.collectionImages[imageKey];

		let image = imageDetails;

		this.setState({ imageDetails, currentImage: imageKey });

		let loadingURL =
			this.state.signedUrl['baseUrl'] +
			'loading/' +
			image.givenFileName +
			'?Key-Pair-Id=' +
			this.state.signedUrl.credentials['Key-Pair-Id'] +
			'&Policy=' +
			this.state.signedUrl.credentials['Policy'] +
			'&Signature=' +
			this.state.signedUrl.credentials['Signature'];

		let optimizedURL =
			this.state.signedUrl['baseUrl'] +
			'optimized/' +
			image.givenFileName +
			'?Key-Pair-Id=' +
			this.state.signedUrl.credentials['Key-Pair-Id'] +
			'&Policy=' +
			this.state.signedUrl.credentials['Policy'] +
			'&Signature=' +
			this.state.signedUrl.credentials['Signature'];

		let loadingImage = new Image();
		loadingImage.src = loadingURL;
		loadingImage.onload = () => {
			let loadingImageDownloaded = this.state.loadingImageDownloaded;
			loadingImageDownloaded[imageDetails.image_id] = true;
			this.setState({
				loadingImageDownloaded,
			});
		};

		let optimizedImage = new Image();
		optimizedImage.src = optimizedURL;
		optimizedImage.onload = () => {
			let fullImageDownloaded = this.state.fullImageDownloaded;
			fullImageDownloaded[imageDetails.image_id] = true;

			this.setState({
				fullImageDownloaded,
				shouldDisplayGrid: true,
			});
		};

		this.props.router.replace(
			`/${this.props.query.projectSlug}/favourites/${this.props.query.favouritesID}/${imageDetails.image_id}`
		);
	};

	renderImageSlideShow = () => {
		var settings = {
			...this.state.settings,
			initialSlide: this.state.initialSlide,
			beforeChange: (current, next) => this.beforeSlideChange(next),
		};

		let gallery = this.state.collectionImages;

		return (
			<Slider {...settings} ref={(slider) => (this.slider = slider)}>
				{_.map(gallery, (imageDetails, key) => {
					let image = imageDetails;

					let fullUrl =
						this.state.signedUrl['baseUrl'] +
						'optimized/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					let loadingUrl =
						this.state.signedUrl['baseUrl'] +
						'optimized/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					return (
						<div className="image-wrapper">
							{_.has(
								this.state.fullImageDownloaded,
								imageDetails.image_id
							) ? (
								<img
									src={fullUrl}
									alt={
										'full-image ' +
										imageDetails.givenFileName
									}
									onClick={() => this.toggleActionbar()}
								/>
							) : _.has(
									this.state.loadingImageDownloaded,
									imageDetails._id
							  ) ? (
								<img
									src={loadingUrl}
									alt={
										'loading-image ' +
										imageDetails.givenFileName
									}
									onClick={() => this.toggleActionbar()}
								/>
							) : (
								<Loader />
							)}
						</div>
					);
				})}
			</Slider>
		);
	};

	renderImageGrid = () => {
		var settings = {
			...this.state.thumbSettings,
			initialSlide: this.state.initialSlide,
			//beforeChange: (current, next) => this.beforeSlideChange(next),
		};

		let gallery = this.state.collectionImages;

		return (
			<Slider
				{...settings}
				slidesToScroll={this.state.slidesToScrollThumbs}
			>
				{_.map(gallery, (imageDetails, key) => {
					let image = imageDetails;

					let thumbURl =
						this.state.signedUrl['baseUrl'] +
						'thumbnails-100h/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					return (
						<div
							className="image-wrapper"
							onClick={() => this.goToSlick(key)}
						>
							<img
								src={thumbURl}
								alt={
									'thumbnails-image ' +
									imageDetails.givenFileName
								}
							/>
						</div>
					);
				})}
			</Slider>
		);
	};

	render() {
		return (
			<div className="wrapper">
				<div className="container">
					<div className="image-container w-100p">
						{this.state.isCollectionLoading ||
						this.state.isGetSignedURL ||
						this.state.isLoading ? (
							<Loader />
						) : (
							this.renderImageSlideShow()
						)}
					</div>
					<div
						className={
							this.state.showActionbar
								? 'header-versions-wrapper slidetopDown'
								: 'header-versions-wrapper slidetopUp'
						}
					>
						<Header
							title={this.state.imageDetails.displayName}
							dateTimeEpoch={
								this.state.imageDetails.dateTimeEpoch
							}
							screenName={'fullView-favts'}
							onClick={() =>
								this.props.router.replace(
									`/${this.props.query.projectSlug}/favourites/${this.props.query.favouritesID}`
								)
							}
						/>
					</div>
					<div
						className={
							this.state.showActionbar
								? 'thumbnails-actionbar-wrapper slidebottomUp'
								: 'thumbnails-actionbar-wrapper slidebottomDown'
						}
					>
						<div className="thumbnails-wrapper w-100p">
							{this.state.isCollectionLoading ||
							this.state.isGetSignedURL ||
							this.state.isLoading ||
							this.state.shouldDisplayGrid === false
								? ''
								: this.renderImageGrid()}
						</div>
						<div className="action-bar w-100p">
							<a className="icon"></a>
							<a
								onClick={() => this.toggleFavourites()}
								className="icon"
							>
								<Fav />
							</a>
							<a className="icon"></a>
						</div>
					</div>

					<Modal
						handleClose={() =>
							this.state.isDisplayFavourites
								? this.toggleFavourites()
								: this.state.isDisplaySuggestEdit
								? this.toggleSuggestEdit()
								: ''
						}
						show={
							this.state.isDisplayFavourites ||
							this.state.isDisplaySuggestEdit
						}
						modalType={
							this.state.isDisplayFavourites
								? 'right'
								: this.state.isDisplaySuggestEdit
								? 'bottom'
								: ''
						}
					>
						{this.state.isDisplayFavourites ? (
							<RightModalLayout
								onClose={() => this.toggleFavourites()}
							>
								<Collections
									collectionType={'add'}
									imageID={this.state.imageDetails.image_id}
									tenantID={this.state.tenantID}
								/>
							</RightModalLayout>
						) : this.state.isDisplaySuggestEdit ? (
							<BottomModalLayout type={'bottom'}>
								<SuggestedEditsModal
									handleClose={() => this.toggleSuggestEdit()}
								/>
							</BottomModalLayout>
						) : (
							''
						)}
					</Modal>
				</div>
			</div>
		);
	}
}

export default withRouter(FavouritesFullImage);
