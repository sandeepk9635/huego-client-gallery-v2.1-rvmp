import React, { Component } from 'react';

import ProjectController from '../../../../src/controllers/project';

import Header from '../../../../src/views/components/header/header';
import SubHeader from '../../../../src/views/components/header/subheader';
import _ from 'lodash';

import { withRouter } from 'next/router';

import Loader from '../../../../src/views/components/global/loader';
import CollectionEmptyState from '../../../../src/views/components/uicomponents/emptystates/collectionsEmpty';

import dynamic from 'next/dynamic';

const MasonryGallery = dynamic(
	() =>
		import('../../../../src/views/components/uicomponents/masonryGallery'),
	{
		ssr: false,
	}
);

class FavouritesGallery extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			signedUrl: {},

			isGetSignedURL: true,
			isLoading: true,

			isCollectionLoading: true,
			showEmptyState: false,

			visible: true,
			imagesStatus: {},
			isImageStatusSet: false,
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = tenantID[0] !== 'localhost' ? 'bhogesh' : tenantID[0];
		this.setState({
			tenantID,
			prevScrollpos: window.pageYOffset,
			collectionID: this.props.query.favouritesID,
		});

		this.validateUser();

		window.addEventListener('scroll', this.handleScroll);
		document.body.style.overflow = 'scroll';

		this.getSignedURLForProject();
		this.getCollections();
		this.getCollectionImages(this.props.query.favouritesID);
	};

	componentWillUnmount = () => {
		window.removeEventListener('scroll', this.handleScroll);
	};

	componentDidUpdate = () => {
		if (
			this.state.isCollectionLoading === false &&
			this.state.isGetSignedURL === false &&
			this.state.isLoading === false &&
			this.state.isImageStatusSet === false
		) {
			let imagesStatus = {};
			let imageCount = 0;

			_.map(this.state.collectionImages, (image) => {
				_.map(this.state.collections, (collection) => {
					if (
						_.indexOf(collection.image_ids, image['image_id']) >= 0
					) {
						if (
							_.has(
								imagesStatus[image['image_id']],
								'collections'
							)
						) {
							imagesStatus[image['image_id']]['collections'] =
								imagesStatus[image['image_id']]['collections'] +
								1;
						} else {
							imagesStatus[image['image_id']] = [];
							imagesStatus[image['image_id']]['collections'] = 1;
						}
					}
				});
				imageCount = imageCount + 1;
			});

			if (_.size(this.state.collectionImages) === imageCount) {
				this.setState({ imagesStatus, isImageStatusSet: true });
			}
		}
	};

	handleScroll = () => {
		const { prevScrollpos } = this.state;

		const currentScrollPos = window.pageYOffset;
		const visible = prevScrollpos > currentScrollPos;

		this.setState({
			prevScrollpos: currentScrollPos,
			visible,
		});
	};

	shuffleBetweenCollections = (collectionID) => {
		this.setState({
			collectionID,
			isLoading: true,
			isImageStatusSet: false,
			collectionImages: {},
			imagesStatus: {},
		});
		this.props.router.replace(
			`/${this.props.query.projectSlug}/favourites/${collectionID}`
		);
		this.getCollectionImages(collectionID);
	};

	render() {
		return (
			<div>
				<Header
					screenName={'favourites'}
					onClick={() => this.props.router.back()}
					title={'Favourites'}
					onscrollclassname={this.state.visible ? '' : 'hidden'}
				/>

				{this.state.isCollectionLoading ? (
					''
				) : (
					<SubHeader
						tagsJson={this.state.collections}
						isActive={this.state.collectionID}
						onClick={(collectionID) =>
							this.shuffleBetweenCollections(collectionID)
						}
						onscrollclassname={this.state.visible ? '' : 'hidden'}
					/>
				)}

				<div className="gallery-wrapper">
					{this.state.isCollectionLoading ||
					this.state.isGetSignedURL ||
					this.state.isLoading ? (
						<Loader />
					) : this.state.showEmptyState === true ? (
						<CollectionEmptyState emptyalbum={true} />
					) : (
						<MasonryGallery
							view={'favourites'}
							images={this.state.collectionImages}
							selectedGallery={this.state.collectionID}
							signedUrl={this.state.signedUrl}
							navigateImage={(imageID) =>
								this.props.router.replace(
									`/${this.props.query.projectSlug}/favourites/${this.state.collectionID}/${imageID}`
								)
							}
							imagesStatus={this.state.imagesStatus}
						/>
					)}
				</div>
			</div>
		);
	}
}

export default withRouter(FavouritesGallery);
