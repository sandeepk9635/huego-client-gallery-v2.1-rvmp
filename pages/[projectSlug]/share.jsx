import React, { Component } from 'react';
import Header from '../../src/views/components/header/header';
import Privacyicon from '../../src/assets/svg/lock.svg';
import Downloadicon from '../../src/assets/svg/download.svg';
import Linkicon from '../../src/assets/svg/link.svg';
import Keyicon from '../../src/assets/svg/key.svg';
import Copyicon from '../../src/assets/svg/copy.svg';
import Shareicon from '../../src/assets/svg/share.svg';
import Unlockicon from '../../src/assets/svg/privacy.svg';
import Lockicon from '../../src/assets/svg/unlock-no-border.svg';

import { withRouter } from 'next/router';
import ProjectController from '../../src/controllers/project';

import Loader from '../../src/views/components/global/loader';
import _ from 'lodash';

class ProjectShare extends ProjectController {
	constructor() {
		super();
		this.state = {
			screenName: 'projectShare',
			isguestSharingEnabled: false,

			projectDetails: {},
			projectDetailsShare: {
				guestAccess: {},
			},
			isLoading: true,
			isProjectDetailsShareLoading: true,
			isGetSignedURL: true,

			isRemovePin: false,
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		this.getProjectDetails();
		this.getSignedURLForProject();
		this.getProjectShareDetails();

		this.validateUser();
	};
	toggleGuestSharing = () => {
		this.setState({
			isguestSharingEnabled: !this.state.isguestSharingEnabled,
		});
	};

	copyTextFunction = (type) => {
		if (type === 'message') {
			this.setState({
				isTextCopied: true,
			});
			navigator.clipboard.writeText(
				this.state.projectDetails.guestAccess.pin === null
					? 'Hi there, here are our photos from ' +
							this.state.projectDetails.title +
							' https://' +
							this.state.tenantID +
							'.huego.com/' +
							this.props.query.projectSlug
					: 'Hi there, here are our photos from ' +
							this.state.projectDetails.title +
							' https://' +
							this.state.tenantID +
							'.huego.com/' +
							this.props.query.projectSlug +
							'. Guest PIN “' +
							this.state.projectDetails.guestAccess.pin +
							'”.'
			);
		}

		if (type === 'link') {
			this.setState({
				isLinkCopied: true,
			});
			navigator.clipboard.writeText(
				'https://' +
					this.state.tenantID +
					'.huego.com/' +
					this.props.query.projectSlug
			);
		}

		if (type === 'pin') {
			navigator.clipboard.writeText(
				this.state.projectDetails.guestAccess.pin
			);
		}
	};

	renderAlbumThumbnails = () => {
		return _.map(this.state.projectDetails.albums, (album, key) => {
			if (album.isPublished === true)
				return (
					<div className="album-item w-100p f-left d-flex align-center">
						<div className="w-50p f-left item-left d-flex align-center">
							<div className="cover-pic">
								{album.coverImage._id === null ? (
									<div>{album.title[0]}</div>
								) : (
									<img
										src={
											this.state.signedUrl['baseUrl'] +
											'thumbnails-100h/' +
											album.coverImage.givenFileName +
											'?Key-Pair-Id=' +
											this.state.signedUrl['credentials'][
												'Key-Pair-Id'
											] +
											'&Signature=' +
											this.state.signedUrl['credentials'][
												'Signature'
											] +
											'&Policy=' +
											this.state.signedUrl['credentials'][
												'Policy'
											]
										}
									/>
								)}
							</div>
							<div className="text">
								<div className="text-top">{album.title}</div>
								{/* <div className="text-bottom">103 photos</div> */}
							</div>
						</div>
						<div className="w-50p f-left item-right">
							{album.guestAccess.isEnabled === false ? (
								<a
									className="f-right d-flex align-center"
									onClick={() =>
										this.updateAlbumPrivacy(true, album._id)
									}
								>
									<Unlockicon />
									Private
								</a>
							) : (
								<a
									className="f-right d-flex align-center"
									onClick={() =>
										this.updateAlbumPrivacy(
											false,
											album._id
										)
									}
								>
									<Lockicon />
									Public
								</a>
							)}
						</div>
					</div>
				);
		});
	};

	renderGuestPinSharing = () => {
		return (
			<div className="share-item w-100p f-left">
				<div className="action-text f-left">
					<div className="w-100p f-left bottom d-flex align-center">
						<div className="action-icon f-left d-flex align-center">
							<Privacyicon />
						</div>
						<div className="action-sub-text">
							Enable PIN Sharing
						</div>
					</div>
				</div>
				<div className="action-button f-left">
					<div className="f-right">
						<label className="form-switch">
							{this.state.isRemovePin === true ? (
								<Loader type={'image'} bg={false} />
							) : (
								<input
									type="checkbox"
									checked={
										_.has(
											this.state.projectDetailsShare
												.guestAccess,
											'pin'
										) &&
										this.state.projectDetailsShare
											.guestAccess.pin !== null
											? true
											: false
									}
									onChange={(e) =>
										this.toggleGuestProjectPin(
											_.has(
												this.state.projectDetailsShare
													.guestAccess,
												'pin'
											) &&
												this.state.projectDetailsShare
													.guestAccess.pin === null
												? true
												: false,
											'removePin'
										)
									}
								/>
							)}
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		);
	};

	renderDonwloadOptions = () => {
		return (
			<div className="share-item w-100p f-left">
				<div className="action-text f-left">
					<div className="w-100p f-left bottom d-flex align-center">
						<div className="action-icon f-left d-flex align-center">
							<Downloadicon />
						</div>
						<div className="action-sub-text">
							Allow Guests to Download Original Images
						</div>
					</div>
				</div>
				<div className="action-button f-left">
					<div className="f-right">
						<label className="form-switch">
							<input
								type="checkbox"
								checked={
									this.state.isguestSharingEnabled
										? true
										: false
								}
								onChange={(e) => this.toggleGuestSharing(e)}
							/>
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		);
	};

	render() {
		if (
			this.state.isLoading === true ||
			this.state.isProjectDetailsShareLoading === true
		) {
			return (
				<div style={{ height: 200 }}>
					<Loader type={'image'} bg={false} />
				</div>
			);
		} else
			return (
				<div className="wrapper">
					<div className="container">
						<Header
							screenName={this.state.screenName}
							title={'Share Project'}
							onClick={() =>
								this.props.router.replace(
									`/${this.props.query.projectSlug}`
								)
							}
						/>
						<div className=" share-wrapper">
							<div className="help-text-wrapper w-100p f-left">
								Using the guest access link people can access
								all the public albums. Hidden photos in albums
								are secured and won’t be visible to anyone but
								you.
							</div>
							<div className="main-share-enable w-100p f-left d-flex align-center">
								<div className="action-text f-left">
									<div className="f-left w-100p top">
										Enable Guest Sharing
									</div>
									<div className="w-100p f-left bottom">
										By enabling guest sharing anyone with
										link can access to all the albums which
										are in public.
									</div>
								</div>
								<div className="action-button f-left">
									<div className="f-right">
										<label className="form-switch">
											<input
												type="checkbox"
												checked={
													this.state
														.projectDetailsShare
														.guestAccess.isEnabled
														? true
														: false
												}
												onChange={(e) =>
													this.toggleGuestAccessEnableForProject(
														!this.state
															.projectDetailsShare
															.guestAccess
															.isEnabled
													)
												}
											/>
											<span class="slider round"></span>
										</label>
									</div>
								</div>
							</div>
							{/* Enable PIN Sharing  */}
							{this.state.projectDetailsShare.guestAccess
								.isEnabled ? (
								<React.Fragment>
									{this.renderGuestPinSharing()}
									{/*
								Downloadable
								Feature not available now

							this.renderDonwloadOptions()

								*/}
									{/* Link */}
									<div className="share-item w-100p f-left">
										<div className="action-text f-left">
											<div className="w-100p f-left bottom d-flex align-center">
												<div className="action-icon f-left d-flex align-center">
													<Linkicon />
												</div>
												<div className="action-sub-text">
													<span className="w-100p f-left sub-text-title">
														Link
													</span>
													<span className="link w-100p f-left">
														{this.state.tenantID}
														.huego.com/
														{
															this.props.query
																.projectSlug
														}
													</span>
												</div>
											</div>
										</div>
										<div className="action-button f-left">
											<a
												className="f-right"
												onClick={() =>
													this.copyTextFunction(
														'link'
													)
												}
											>
												<Copyicon />
											</a>
										</div>
									</div>
									{/* RESET PIN */}

									{this.state.projectDetailsShare.guestAccess
										.pin === null ? (
										''
									) : (
										<div className="share-item w-100p f-left">
											<div className="action-text f-left pin-text">
												<div className="w-100p f-left bottom d-flex align-center">
													<div className="action-icon f-left d-flex align-center">
														<Keyicon />
													</div>
													<div className="action-sub-text">
														<span className="w-100p f-left sub-text-title">
															PIN
														</span>
														{_.has(
															this.state
																.projectDetailsShare
																.guestAccess,
															'pin'
														) ? (
															<span className="w-100p f-left">
																{
																	this.state
																		.projectDetailsShare
																		.guestAccess
																		.pin
																}
															</span>
														) : (
															''
														)}
													</div>
												</div>
											</div>
											<div className="action-button f-left pin-button">
												<a className="d-flex align-center ">
													{this.state.isResetPin ===
													true ? (
														<Loader
															type={'image'}
															bg={false}
														/>
													) : (
														<span
															onClick={() =>
																this.toggleGuestProjectPin(
																	true,
																	'resetPin'
																)
															}
														>
															Reset PIN
														</span>
													)}

													<Copyicon
														onClick={() =>
															this.copyTextFunction(
																'pin'
															)
														}
													/>
												</a>
											</div>
										</div>
									)}
									{/* Link with PIN */}
									<div className="share-item w-100p f-left">
										<div className="action-text f-left ">
											<div className="w-100p f-left bottom d-flex align-center">
												<div className="action-icon f-left d-flex align-center">
													<Shareicon />
												</div>
												<div className="action-sub-text">
													<span className="w-100p f-left sub-text-title">
														Copy Link and PIN to
														clipboard
													</span>
													<span className="w-100p f-left mt-4 link-text">
														Hi there, Here are our
														photos from our{' '}
														{
															this.state
																.projectDetails
																.title
														}{' '}
														<a>
															{
																this.state
																	.tenantID
															}
															.huego.com/
															{
																this.props.query
																	.projectSlug
															}{' '}
														</a>
														{_.has(
															this.state
																.projectDetails
																.guestAccess,
															'pin'
														) &&
														this.state
															.projectDetails
															.guestAccess.pin !==
															null
															? `Use the PIN: ${this.state.projectDetails.guestAccess.pin}`
															: ''}
													</span>
												</div>
											</div>
										</div>
										<div className="action-button f-left ">
											<a
												className="f-right"
												onClick={() =>
													this.copyTextFunction(
														'message'
													)
												}
											>
												<Copyicon />
											</a>
										</div>
									</div>
									{/* Album Privacy */}
									<div className="help-text-sub w-100p f-left">
										The albums in <b>Public</b> will be
										visibile to all the guest users. The
										albums in <b>Private</b> will be
										visibile to all the guest users.
									</div>
									{_.size(
										this.state.projectDetails.albums === 0
									) ? (
										<p>No Albums</p>
									) : this.state.isGetSignedURL === true ||
									  this.state.isLoading === true ||
									  this.state
											.isProjectDetailsShareLoading ===
											true ? (
										<div style={{ height: 200 }}>
											<Loader type={'image'} bg={false} />
										</div>
									) : (
										this.renderAlbumThumbnails()
									)}
								</React.Fragment>
							) : (
								''
							)}
						</div>
					</div>
				</div>
			);
	}
}

export default withRouter(ProjectShare);
