import React, { Component } from 'react';

import Header from '../../../src/views/components/header/header';

import BgWrapper from '../../../src/views/components/global/bgWrapper';
import InfoWrapper from '../../../src/views/components/global/infoWrapper';
import AlbumThumbnailsWrapper from '../../../src/views/components/uielements/albumThumbnailsWrapper';
import RightModalLayout from '../../../src/views/components/uicomponents/modals/layouts/rightModal';

import Collections from '../../../src/views/components/uicomponents/collections.jsx';

import ProjectController from '../../../src/controllers/project';

import _ from 'lodash';

import { withRouter } from 'next/router';
import Loader from '../../../src/views/components/global/loader';
import Slider from 'react-slick';
import Modal from '../../../src/views/components/uicomponents/modals/index';
import BottomModalLayout from '../../../src/views/components/uicomponents/modals/layouts/modalLayout';
import FeedbackModal from '../../../src/views/components/uicomponents/modals/feedbackModal';

import * as ProjectAction from '../../../src/controllers/actions';
import { saveAs } from 'file-saver';

const JSZip = require('jszip');
const zipDownloadSize = 1000000000;

class Album extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			projectDetails: { albums: [] },
			signedUrl: {},

			isGetSignedURL: true,
			isLoading: true,
			isSetAlbumKey: false,
			settings: {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
			},
			galleryImages: {},
			albumDetails: {},
			isGalleryLoading: true,

			isDisplayFavourites: false,
			isDisplayShare: false,

			screenName: 'albumHome',
			windowWidth: 0,

			imageDownloadedCount: 0,
			imagesDownloaded: [],

			isDownloadLoading: false,
			imagesStatus: {},

			zipDownloaded: 0,
			imageDownloadedSize: 0,
		};
		this.slider = React.createRef();

		this.albumScreen = React.createRef();
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
			albumID: this.props.query.albumID,
		});

		this.validateUser();

		this.getProjectDetails();
		this.getSignedURLForProject();
		this.getAlbumDetails(this.props.query.albumID);
		//this.getAlbumImages(this.props.query.albumID);
		this.setState({
			windowWidth: window.innerWidth,
		});
	};

	componentDidUpdate = () => {
		if (
			_.has(this.props.query, 'albumID') &&
			_.size(this.state.projectDetails.albums) > 0 &&
			this.state.isSetAlbumKey === false &&
			this.state.screenName === 'albumHome'
		) {
			let albumID = this.props.query.albumID;
			let activeAlbumKey = _.findIndex(
				this.state.projectDetails.albums,
				function (album) {
					return album.slug == albumID;
				}
			);

			this.setState({
				activeAlbumKey,
				isSetAlbumKey: true,
				albumTitle: this.state.projectDetails.albums[activeAlbumKey][
					'title'
				],
				albumID: this.props.query.albumID,
			});
		}
	};

	goToAlbum = (albumKey) => {
		this.setState({ screenName: 'albumHome', activeAlbumKey: albumKey });

		let album = this.state.projectDetails.albums[albumKey];
		this.props.router.push(`/${this.props.projectData.slug}/${album.slug}`);
		this.getAlbumDetails(album.slug);
		this.slider.slickGoTo(albumKey);

		this.setState({
			albumTitle: album['title'],
			albumID: album.slug,
			isGalleryLoading: true,
		});
	};

	goToScreen = (screenName, imageID = null) => {
		if (screenName === 'gallery') {
			this.props.router.push(
				`/${this.props.projectData.slug}/${this.state.albumID}/${this.state.albumDetails.sections[0]['_id']}`
			);
		}

		if (screenName === 'albumHome') {
			this.props.router.push(
				`/${this.props.projectData.slug}/${this.state.albumID}`
			);
		}

		if (screenName === 'projectHome') {
			this.props.router.push(`/${this.props.projectData.slug}`);
		}
	};

	renderAlbumSlick = () => {
		return _.map(this.state.projectDetails.albums, (album, key) => {
			if (album.isPublished)
				return (
					<div key={key} className="bg-info-wrapper">
						<BgWrapper
							bgImage={
								this.state.signedUrl['baseUrl'] +
								'thumbnails-300w/' +
								album.coverImage.givenFileName +
								'?Key-Pair-Id=' +
								this.state.signedUrl['credentials'][
									'Key-Pair-Id'
								] +
								'&Signature=' +
								this.state.signedUrl['credentials'][
									'Signature'
								] +
								'&Policy=' +
								this.state.signedUrl['credentials']['Policy']
							}
						/>
						<InfoWrapper
							title={album.title}
							albumsCount={album.imagesCount}
							screenName={this.state.screenName}
							{...this.props}
							onClickViewPhotos={() => this.goToScreen('gallery')}
							albumDetails={album}
							updateAlbumDetails={() =>
								this.getProjectDetails(album._id)
							}
							isLoadingPrivacy={this.state.isLoadingPrivacy}
							downloadAlbumImages={() =>
								this.downloadAlbumImages(album.imagesCount)
							}
							isDownloadLoading={this.state.isDownloadLoading}
							onClickRouteToShare={() =>
								this.props.router.push(
									`/${this.props.query.projectSlug}/${album.slug}/share`
								)
							}
							allowDownloads={
								this.state.projectDetails
									.canClientDownloadOriginals
							}
						/>
						<div className="gradient-thumbs"></div>
					</div>
				);
		});
	};
	renderSlickSlider = () => {
		const settings = {
			...this.state.settings,
			initialSlide: this.state.activeAlbumKey,
			beforeChange: (current, next) => this.goToAlbum(next),
		};
		if (
			this.state.isGetSignedURL === true ||
			this.state.isLoading === true
		) {
			null;
		} else {
			return (
				<div className={'pages-slick-slider-wrapper w-100p'}>
					<Slider
						{...settings}
						ref={(slider) => (this.slider = slider)}
					>
						{this.renderAlbumSlick()}
					</Slider>
				</div>
			);
		}
	};

	toggleFavourites = () => {
		this.setState({
			isDisplayFavourites: !this.state.isDisplayFavourites,
		});
	};

	toggleFeedback = (getData = null) => {
		this.setState({
			isDisplayShare: !this.state.isDisplayShare,
		});

		if (getData !== null) {
			this.getAlbumDetails(this.state.albumID);
		}
	};

	downloadAlbumImages = async (imageCount) => {
		this.setState({
			isDownloadLoading: true,
			isDownloadModal: false,
			downloadCount: imageCount,
		});

		let response = await this.getPermissionToDownloadAlbum();
		_.map(response, (imageSingedUrl) => {
			this.downloadSingleImage(imageSingedUrl);
		});
	};

	zipDownloadedFiles = async () => {
		this.setState({
			isZipping: true,
		});
		let count = 0;
		let imagesSize = 0;

		var zip;

		var album;

		let totalZips = Math.ceil(
			this.state.imageDownloadedSize / zipDownloadSize
		);

		await _.map(this.state.imagesDownloaded, async (images) => {
			if (imagesSize === 0) {
				zip = new JSZip();
				album = zip.folder(this.state.albumDetails.title);
			}

			let response = images['data'];
			let responseSize = images['size'];

			imagesSize = responseSize + imagesSize;
			album.file(images.displayName, response, {
				binary: true,
			});
			count++;

			if (
				imagesSize >= zipDownloadSize ||
				count === this.state.downloadCount
			) {
				imagesSize = 0;
				let zipFileName = this.state.albumDetails.title + '.zip';
				await zip
					.generateAsync({ type: 'blob' })
					.then(function callback(blob) {
						saveAs(blob, zipFileName);
					})
					.then(() => {
						let zipDownloaded = this.state.zipDownloaded + 1;
						this.setState({
							zipDownloaded,
						});

						if (zipDownloaded === totalZips) {
							this.setState({
								isDownloadLoading: false,
								imagesDownloaded: {},
								imageDownloadedCount: 0,
								isZipping: false,
							});
						}
					});
			}
		});
	};

	downloadSingleImage = async (imageSingedUrl) => {
		let imageResponse = await ProjectAction.downloadImageFromAWSS3(
			imageSingedUrl.signedUrl
		);

		if (imageResponse[0] === true) {
			let imageDownloadedCount = this.state.imageDownloadedCount + 1;
			let imagesDownloaded = this.state.imagesDownloaded;
			let imageDownloadedSize =
				this.state.imageDownloadedSize + imageResponse[1];

			let json = {
				data: imageResponse[2],
				size: imageResponse[1],
				displayName: imageSingedUrl.displayName,
			};
			imagesDownloaded.push(json);

			this.setState({
				imageDownloadedCount,
				imagesDownloaded,
				imageDownloadedSize,
			});

			if (imageDownloadedCount === this.state.downloadCount) {
				this.zipDownloadedFiles();
			}
		}
	};

	render() {
		return (
			<div className="wrapper">
				<div className={`container `} ref={this.container}>
					<Header
						screenName={this.state.screenName}
						onClick={(screenName) => this.goToScreen(screenName)}
						title={this.state.albumDetails.title}
						showFeedBack={this.state.projectDetails.canClientReview}
						toggleFavourites={() => this.toggleFavourites()}
						toggleFeedback={() => this.toggleFeedback()}
					/>
					<div className="page-wrapper">
						<div ref={this.albumScreen}>
							{this.renderSlickSlider()}
						</div>

						{_.size(this.state.projectDetails.albums === 0) ? (
							<p>ALbums Empty State</p>
						) : this.state.isGetSignedURL === true ||
						  this.state.isLoading === true ? (
							<div className={'albums-thumbnails-wrapper'}>
								<Loader type={'image'} bg={false} />
							</div>
						) : (
							<AlbumThumbnailsWrapper
								albums={this.state.projectDetails.albums}
								signedUrl={this.state.signedUrl}
								onClick={(key) => this.goToAlbum(key)}
								isActive={this.state.activeAlbumKey}
								windowWidth={this.state.windowWidth}
							/>
						)}
					</div>
					<Modal
						handleClose={() => this.toggleFavourites()}
						show={this.state.isDisplayFavourites}
						modalType={'right'}
					>
						<RightModalLayout
							onClose={() => this.toggleFavourites()}
						>
							<Collections
								collectionType={'view'}
								projectSlug={this.props.query.projectSlug}
							/>
						</RightModalLayout>
					</Modal>
					<Modal
						handleClose={() => this.toggleFeedback()}
						show={this.state.isDisplayShare}
						modalType={
							this.state.windowWidth >= 768 ? 'right' : 'bottom'
						}
					>
						<BottomModalLayout type={'bottom'}>
							<FeedbackModal
								handleClose={(e) => this.toggleFeedback(e)}
								projectSlug={this.props.query.projectSlug}
								albumID={this.props.query.albumID}
								reviewData={this.state.albumDetails.reviews}
							/>
						</BottomModalLayout>
					</Modal>
				</div>
			</div>
		);
	}
}

export default withRouter(Album);
