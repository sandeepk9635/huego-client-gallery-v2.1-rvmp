import React, { Component } from 'react';

import Header from '../../../../src/views/components/header/header';
import Download from '../../../../src/assets/svg/download.svg';
import Fav from '../../../../src/assets/svg/fav.svg';
import Unlock from '../../../../src/assets/svg/unlock.svg';
import Visible from '../../../../src/assets/svg/visible.svg';
import InVisible from '../../../../src/assets/svg/invisible.svg';
import Versions from '../../../../src/assets/svg/versions.svg';
import Slider from 'react-slick';

import ProjectController from '../../../../src/controllers/project';

import _ from 'lodash';
import { withRouter } from 'next/router';
import Loader from '../../../../src/views/components/global/loader';

import Modal from '../../../../src/views/components/uicomponents/modals/index';
import SuggestedEditsModal from '../../../../src/views/components/uicomponents/modals/suggestedEditsModal';
import VersionsModal from '../../../../src/views/components/uicomponents/modals/versionsModal';
import BottomModalLayout from '../../../../src/views/components/uicomponents/modals/layouts/modalLayout';
import RightModalLayout from '../../../../src/views/components/uicomponents/modals/layouts/rightModal';

import Collections from '../../../../src/views/components/uicomponents/collections.jsx';

import RightArrow from '../../../../src/assets/svg/rightarrow.svg';
import LeftArrow from '../../../../src/assets/svg/leftarrow.svg';

import jwt_decode from 'jwt-decode';

class ImageFullView extends ProjectController {
	constructor() {
		super();
		this.state = {
			settings: {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				nextArrow: <RightArrow />,
				prevArrow: <LeftArrow />,
				lazyload: true,
			},

			thumbSettings: {
				dots: false,
				infinite: true,
				speed: 500,
				arrows: false,
				centerMode: true,
			},
			thumbArrows: false,
			thumbsToShow: 5,
			showActionbar: false,

			isGetSignedURL: true,
			isLoading: true,

			galleryImages: {},
			galleries: {},
			albumDetails: {},
			imageDetails: {},
			isGalleryLoading: true,

			fullImageDownloaded: [],
			loadingImageDownloaded: [],

			isDisplayFavourites: false,

			activeSlide: 0,

			shouldDisplayGrid: false,

			isDisplayVersions: false,
			suggestEditType: 'bottom',

			isDownloadLoading: false,
			imagesStatus: {},
			isGuestUser: false,
			decoded: {},

			projectDetails: {},
		};
		this.slider = React.createRef();
		this.gridSlider = React.createRef();
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		// if (!localStorage.getItem('usertoken')) {
		// 	return this.props.router.push(
		// 		`/${this.props.query.projectSlug}/login`
		// 	);
		// }

		this.checkUserIsGuest();
		if (localStorage.getItem('usertoken')) {
			let userToken = localStorage.getItem('usertoken');
			var decoded = jwt_decode(userToken);
			this.setState({
				decoded,
			});

			if (decoded.userRole === 'guest' && _.has(decoded, 'album_id')) {
				this.validateUser(true);
				this.setState({
					isGuestUser: true,
				});
				this.getSignedURLForGuestAlbum();
				this.getGuestAlbumImages(
					this.props.query.albumID,
					this.props.query.galleryID
				);
			} else {
				this.validateUser();
				this.getProjectDetails();
				this.getAlbumImages(
					this.props.query.albumID,
					this.props.query.galleryID
				);
				this.getSignedURLForProject();
			}
		} else {
			this.props.router.push(
				`/${this.props.query.projectSlug}/login?from=${this.props.path}`
			);
		}

		if (window.innerWidth >= 768) {
			this.setState({
				thumbsToShow: 16,
				suggestEditType: 'right',
				thumbArrows: true,
			});
		}
	};

	componentDidUpdate = () => {
		if (
			!this.state.isGalleryLoading &&
			!this.state.isGetSignedURL &&
			_.size(this.state.imageDetails) === 0
		) {
			let images = this.state.galleryImages;

			let index = _.findIndex(images, { _id: this.props.query.imageID });

			this.setState({
				initialSlide: index,
				activeThumb: index,
			});
			this.beforeSlideChange(index);
		}
	};

	setImageDetails = () => {
		let images = this.state.galleryImages;
		let imageKey = _.findIndex(images, { _id: this.props.query.imageID });

		let imageDetails = this.state.galleryImages[imageKey];

		let image = _.filter(imageDetails.versions, {
			versionId: imageDetails.activeVersionId,
			isUploaded: true,
			isProcessed: true,
		});

		image = image[0];
		imageDetails = {
			...imageDetails,
			dateTimeEpoch: image.originalDateTime,
		};
		this.setState({ imageDetails });
	};

	toggleActionbar = () => {
		document.body.style.overflow = 'hidden';
		this.setState({
			showActionbar: !this.state.showActionbar,
		});
	};

	lastViewedImageStore = (nextSlide, currentImageID, imageVersionName) => {
		let lastViewedImages = localStorage.getItem('lastViewedImage')
			? JSON.parse(localStorage.getItem('lastViewedImage'))
			: [];

		let lastViewedIndex = _.findIndex(lastViewedImages, {
			projectID: this.props.query.projectSlug,
			albumID: this.props.query.albumID,
			galleryID: this.props.query.galleryID,
		});

		console.log(lastViewedIndex);

		if (lastViewedIndex === -1) {
			lastViewedImages.push({
				projectID: this.props.query.projectSlug,
				albumID: this.props.query.albumID,
				galleryID: this.props.query.galleryID,
				imageID: currentImageID,
			});
		} else {
			lastViewedImages[lastViewedIndex] = {
				projectID: this.props.query.projectSlug,
				albumID: this.props.query.albumID,
				galleryID: this.props.query.galleryID,
				imageID: currentImageID,
			};
		}

		localStorage.setItem(
			'lastViewedImage',
			JSON.stringify(lastViewedImages)
		);
	};

	beforeSlideChange = (imageKey) => {
		let imageDetails = this.state.galleryImages[imageKey];

		let image = _.filter(imageDetails.versions, {
			versionId: imageDetails.activeVersionId,
			isUploaded: true,
			isProcessed: true,
		});

		image = image[0];
		imageDetails = {
			...imageDetails,
			dateTimeEpoch: image.originalDateTime,
		};
		this.setState({ imageDetails });

		let loadingURL =
			this.state.signedUrl['baseUrl'] +
			'loading/' +
			image.givenFileName +
			'?Key-Pair-Id=' +
			this.state.signedUrl.credentials['Key-Pair-Id'] +
			'&Policy=' +
			this.state.signedUrl.credentials['Policy'] +
			'&Signature=' +
			this.state.signedUrl.credentials['Signature'];

		let optimizedURL =
			this.state.signedUrl['baseUrl'] +
			'optimized/' +
			image.givenFileName +
			'?Key-Pair-Id=' +
			this.state.signedUrl.credentials['Key-Pair-Id'] +
			'&Policy=' +
			this.state.signedUrl.credentials['Policy'] +
			'&Signature=' +
			this.state.signedUrl.credentials['Signature'];

		let loadingImage = new Image();
		loadingImage.src = loadingURL;
		loadingImage.onload = () => {
			let loadingImageDownloaded = this.state.loadingImageDownloaded;
			loadingImageDownloaded[imageDetails._id] = true;
			this.setState({
				loadingImageDownloaded,
			});
		};

		let optimizedImage = new Image();
		optimizedImage.src = optimizedURL;
		optimizedImage.onload = () => {
			let fullImageDownloaded = this.state.fullImageDownloaded;
			fullImageDownloaded[imageDetails._id] = true;

			this.setState({
				fullImageDownloaded,
				shouldDisplayGrid: true,
			});
		};

		if (this.state.shouldDisplayGrid === true)
			this.gridSlider.slickGoTo(imageKey);

		this.lastViewedImageStore(
			imageKey,
			imageDetails._id,
			image.givenFileName
		);
		this.setState({ activeThumb: imageKey });

		this.props.router.replace(
			`/${this.props.query.projectSlug}/${this.props.query.albumID}/${this.props.query.galleryID}/${imageDetails._id}`
		);
	};

	renderImageSlideShow = () => {
		var settings = {
			...this.state.settings,
			initialSlide: this.state.initialSlide,
			beforeChange: (current, next) => this.beforeSlideChange(next),
		};

		let gallery = this.state.galleryImages;

		return (
			<Slider
				{...settings}
				arrows={this.state.thumbArrows}
				ref={(slider) => (this.slider = slider)}
			>
				{_.map(gallery, (imageDetails, key) => {
					let image = _.filter(imageDetails.versions, {
						versionId: imageDetails.activeVersionId,
						isUploaded: true,
						isProcessed: true,
					});

					image = image[0];

					let fullUrl =
						this.state.signedUrl['baseUrl'] +
						'optimized/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					let loadingUrl =
						this.state.signedUrl['baseUrl'] +
						'optimized/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					return (
						<div className="image-wrapper">
							{_.has(
								this.state.fullImageDownloaded,
								imageDetails._id
							) ? (
								<img
									src={fullUrl}
									alt={
										'full-image ' +
										imageDetails.givenFileName
									}
									onClick={() => this.toggleActionbar()}
								/>
							) : _.has(
									this.state.loadingImageDownloaded,
									imageDetails._id
							  ) ? (
								<img
									src={loadingUrl}
									alt={
										'loading-image ' +
										imageDetails.givenFileName
									}
									onClick={() => this.toggleActionbar()}
								/>
							) : (
								<Loader />
							)}
						</div>
					);
				})}
			</Slider>
		);
	};

	renderImageGrid = () => {
		var settings = {
			...this.state.thumbSettings,
			initialSlide: this.state.initialSlide,
			slidesToScroll: this.state.thumbsToShow,
			slidesToShow: this.state.thumbsToShow,
			//beforeChange: (current, next) => this.beforeSlideChange(next),
		};

		let gallery = this.state.galleryImages;

		return (
			<Slider
				ref={(gridSlider) => (this.gridSlider = gridSlider)}
				{...settings}
			>
				{_.map(gallery, (imageDetails, key) => {
					let image = _.filter(imageDetails.versions, {
						versionId: imageDetails.activeVersionId,
						isUploaded: true,
						isProcessed: true,
					});

					image = image[0];

					let thumbURl =
						this.state.signedUrl['baseUrl'] +
						'thumbnails-100h/' +
						image.givenFileName +
						'?Key-Pair-Id=' +
						this.state.signedUrl.credentials['Key-Pair-Id'] +
						'&Policy=' +
						this.state.signedUrl.credentials['Policy'] +
						'&Signature=' +
						this.state.signedUrl.credentials['Signature'];

					return (
						<div
							className={
								this.state.activeThumb === key
									? 'image-wrapper thumb-active'
									: 'image-wrapper'
							}
							onClick={() => this.goToSlick(key)}
						>
							<img
								src={thumbURl}
								alt={
									'thumbnails-image ' +
									imageDetails.givenFileName
								}
							/>
						</div>
					);
				})}
			</Slider>
		);
	};

	toggleFavourites = () => {
		this.setState({
			isDisplayFavourites: !this.state.isDisplayFavourites,
		});
	};

	toggleSuggestEdit = () => {
		this.setState({
			isDisplaySuggestEdit: !this.state.isDisplaySuggestEdit,
		});
	};
	toggleVersions = () => {
		this.setState({
			isDisplayVersions: !this.state.isDisplayVersions,
		});
	};

	goToSlick = (k) => {
		this.slider.slickGoTo(k);
	};

	render() {
		return (
			<div className="wrapper">
				<div className="container">
					<div className="image-container w-100p">
						{this.state.isGalleryLoading ||
						this.state.isGetSignedURL ? (
							<Loader />
						) : (
							this.renderImageSlideShow()
						)}
					</div>
					<div
						className={
							this.state.showActionbar
								? 'header-versions-wrapper slidetopDown'
								: 'header-versions-wrapper slidetopUp'
						}
					>
						<Header
							title={this.state.imageDetails.displayName}
							dateTimeEpoch={
								this.state.imageDetails.dateTimeEpoch
							}
							allowSuggestEdit={
								this.state.projectDetails.canClientSuggestEdits
							}
							toggleSuggestEdits={() => this.toggleSuggestEdit()}
							screenName={'fullView'}
							onClick={() =>
								this.props.router.push(
									`/${this.props.query.projectSlug}/${this.props.query.albumID}/${this.props.query.galleryID}`
								)
							}
						/>

						{_.has(this.state.imageDetails, 'versions') === false ||
						_.size(this.state.imageDetails.versions) <= 1 ||
						this.state.decoded.userRole === 'guest' ? (
							''
						) : (
							<div
								className="versions-wrapper  d-flex align-center"
								onClick={() => this.toggleVersions()}
							>
								<div className="icon-with-text">
									<a className="f-left">
										<Versions />
									</a>

									<span className="f-left">
										v
										{_.size(
											this.state.imageDetails.versions
										)}
									</span>
								</div>
							</div>
						)}
					</div>
					<div
						className={
							this.state.showActionbar
								? 'thumbnails-actionbar-wrapper slidebottomUp'
								: 'thumbnails-actionbar-wrapper slidebottomDown'
						}
					>
						<div className="thumbnails-wrapper w-100p">
							{this.state.isGalleryLoading ||
							this.state.isGetSignedURL ||
							this.state.shouldDisplayGrid === false
								? ''
								: this.renderImageGrid()}
						</div>
						<div className="action-bar w-100p">
							{this.state.isDownloadLoading ? (
								<a
									className={
										this.state.decoded.userRole !== 'guest'
											? 'icon'
											: 'icon-guest'
									}
								>
									<span className="loader-download">
										<Loader type={'image'} />
									</span>
								</a>
							) : (
								<a
									className={
										this.state.decoded.userRole !== 'guest'
											? 'icon'
											: 'icon-guest'
									}
									onClick={() => this.downloadThisImage()}
								>
									<Download />
								</a>
							)}

							{this.state.decoded.userRole !== 'guest' ? (
								<React.Fragment>
									<a
										className="icon"
										onClick={() => this.toggleFavourites()}
									>
										<Fav />
									</a>
									<a className="icon">
										{this.state.imageDetails.isHidden ? (
											<InVisible
												onClick={() =>
													this.changeImagePrivacy(
														this.state.imageDetails
															._id,
														!this.state.imageDetails
															.isHidden
													)
												}
											/>
										) : (
											<Visible
												onClick={() =>
													this.changeImagePrivacy(
														this.state.imageDetails
															._id,
														!this.state.imageDetails
															.isHidden
													)
												}
											/>
										)}
									</a>
								</React.Fragment>
							) : (
								''
							)}
						</div>
					</div>

					<Modal
						handleClose={() =>
							this.state.isDisplayFavourites
								? this.toggleFavourites()
								: this.state.isDisplaySuggestEdit
								? this.toggleSuggestEdit()
								: this.state.isDisplayVersions
								? this.toggleVersions()
								: ''
						}
						show={
							this.state.isDisplayFavourites ||
							this.state.isDisplaySuggestEdit ||
							this.state.isDisplayVersions
						}
						modalType={
							this.state.isDisplayFavourites
								? 'right'
								: this.state.isDisplayVersions
								? 'bottom'
								: this.state.isDisplaySuggestEdit
								? this.state.suggestEditType
								: ''
						}
					>
						{this.state.isDisplayFavourites ? (
							<RightModalLayout
								onClose={() => this.toggleFavourites()}
							>
								<Collections
									collectionType={'add'}
									imageID={this.state.imageDetails._id}
									tenantID={this.state.tenantID}
								/>
							</RightModalLayout>
						) : this.state.isDisplaySuggestEdit ||
						  this.state.isDisplayVersions ? (
							<BottomModalLayout type={'bottom'}>
								{this.state.isDisplaySuggestEdit ? (
									<SuggestedEditsModal
										handleClose={() =>
											this.toggleSuggestEdit()
										}
										imageDetails={this.state.imageDetails}
										query={this.props.query}
									/>
								) : (
									<VersionsModal
										handleClose={() =>
											this.toggleVersions()
										}
										imageDetails={this.state.imageDetails}
										signedUrl={this.state.signedUrl}
										query={this.props.query}
									/>
								)}
							</BottomModalLayout>
						) : (
							''
						)}
					</Modal>
				</div>
			</div>
		);
	}
}

export default withRouter(ImageFullView);
