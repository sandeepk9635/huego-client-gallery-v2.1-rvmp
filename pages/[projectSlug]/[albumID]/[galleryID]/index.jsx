import React, { Component } from 'react';

import ProjectController from '../../../../src/controllers/project';

import Header from '../../../../src/views/components/header/header';
import SubHeader from '../../../../src/views/components/header/subheader';

import RightModalLayout from '../../../../src/views/components/uicomponents/modals/layouts/rightModal';

import Collections from '../../../../src/views/components/uicomponents/collections.jsx';

import _ from 'lodash';

import { withRouter } from 'next/router';

import Loader from '../../../../src/views/components/global/loader';

import Modal from '../../../../src/views/components/uicomponents/modals/index';

import dynamic from 'next/dynamic';
import jwt_decode from 'jwt-decode';

const MasonryGallery = dynamic(
	() =>
		import('../../../../src/views/components/uicomponents/masonryGallery'),
	{
		ssr: false,
	}
);

class Gallery extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			projectDetails: { albums: [] },
			signedUrl: {},

			isGetSignedURL: true,
			isLoading: true,

			galleryImages: {},
			galleries: {},
			albumDetails: {},
			isGalleryLoading: true,
			isCollectionLoading: true,

			isDisplayFavourites: false,

			galleryIDs: [],
			isSetGalleries: false,

			visible: true,
			imagesStatus: {},
			isImageStatusSet: true,
			lastViewedImage: null,
			isGuestUser: false,
			decoded: {},
		};
	}

	componentDidMount = () => {
		if (!localStorage.getItem('usertoken')) {
			return this.props.router.push(
				`/${this.props.query.projectSlug}/login?from=${this.props.path}`
			);
		} else {
			let tenantID = window.location.hostname.split('.huego.com');
			tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
			this.setState({
				tenantID,
				prevScrollpos: window.pageYOffset,
			});

			let userToken = localStorage.getItem('usertoken');
			var decoded = jwt_decode(userToken);
			this.setState({
				decoded,
			});
			if (decoded.userRole === 'guest' && _.has(decoded, 'album_id')) {
				this.validateUser(true);
				this.setState({
					isGuestUser: true,
				});
				this.getSignedURLForGuestAlbum();
				this.getGuestAlbumImages(
					this.props.query.albumID,
					this.props.query.galleryID
				);
				this.getGuestAlbumDetails(this.props.query.albumID);
			} else {
				this.validateUser();
				if (decoded.userRole !== 'guest') {
					this.getCollections();
				}
				this.getAlbumDetails(this.props.query.albumID);
				this.getAlbumImages(
					this.props.query.albumID,
					this.props.query.galleryID
				);
				this.getSignedURLForProject();
			}

			window.addEventListener('scroll', this.handleScroll);
			document.body.style.overflow = 'scroll';
		}
	};

	componentWillUnmount = () => {
		window.removeEventListener('scroll', this.handleScroll);
	};

	componentDidUpdate = () => {
		if (
			this.state.isGalleryLoading === false &&
			this.state.isLoading === false &&
			this.state.isSetGalleries === false
		) {
			this.setState({
				isSetGalleries: true,
				galleryID: this.state.albumDetails.sections[0]['_id'],
			});
		}

		if (
			this.state.isGalleryLoading === false &&
			this.state.isCollectionLoading === false &&
			this.state.isImageStatusSet === false
		) {
			let imagesStatus = this.state.imagesStatus;

			let imageCount = 0;
			_.map(this.state.galleryImages, (images) => {
				_.map(this.state.collections, (collection) => {
					if (_.indexOf(collection.image_ids, images._id) >= 0) {
						imagesStatus[images._id]['collections'] =
							imagesStatus[images._id]['collections'] + 1;
					}
				});
				imageCount = imageCount + 1;

				if (this.state.imageCount === imageCount) {
					this.setState({ imagesStatus, isImageStatusSet: true });
				}
			});
		}
	};

	toggleFavourites = () => {
		this.setState({
			isDisplayFavourites: !this.state.isDisplayFavourites,
		});
	};
	handleScroll = () => {
		const { prevScrollpos } = this.state;

		const currentScrollPos = window.pageYOffset;
		const visible = prevScrollpos > currentScrollPos;

		this.setState({
			prevScrollpos: currentScrollPos,
			visible,
		});
	};

	setGalleryID = (galleryID) => {
		this.setState({
			galleryID,
			isGalleryLoading: true,
		});

		this.props.router.replace(
			`/${this.props.query.projectSlug}/${this.props.query.albumID}/${galleryID}`
		);

		if (this.state.isGuestUser === true) {
			this.getGuestAlbumImages(this.props.query.albumID, galleryID);
		} else {
			this.getAlbumImages(this.props.query.albumID, galleryID);
		}
	};

	render() {
		return (
			<div>
				<Header
					screenName={'gallery'}
					onClick={(screenName) =>
						this.props.router.push(
							`/${this.props.query.projectSlug}/${this.props.query.albumID}`
						)
					}
					title={this.state.albumDetails.title}
					toggleFavourites={() => this.toggleFavourites()}
					onscrollclassname={this.state.visible ? '' : 'hidden'}
				/>

				<SubHeader
					tagsJson={this.state.albumDetails.sections}
					isActive={this.props.query.galleryID}
					onClick={(galleryID) => this.setGalleryID(galleryID)}
					onscrollclassname={this.state.visible ? '' : 'hidden'}
				/>

				<div
					className="gallery-wrapper"
					ref={this.state.isGalleryLoading}
				>
					{this.state.isGalleryLoading ||
					this.state.isGetSignedURL ||
					(this.state.isCollectionLoading &&
						this.state.decoded.userRole !== 'guest') ||
					(this.state.decoded.userRole === 'guest' &&
						this.state.isGalleryLoading) ||
					this.state.isGetSignedURL ? (
						<Loader />
					) : (
						<MasonryGallery
							view={'gallery'}
							images={this.state.galleryImages}
							selectedGallery={this.props.query.galleryID}
							signedUrl={this.state.signedUrl}
							navigateImage={(imageID) =>
								this.props.router.push(
									`/${this.props.query.projectSlug}/${this.props.query.albumID}/${this.props.query.galleryID}/${imageID}`
								)
							}
							imagesStatus={this.state.imagesStatus}
							lastViewedImage={this.state.lastViewedImage}
						/>
					)}
				</div>

				<Modal
					handleClose={() => this.toggleFavourites()}
					show={this.state.isDisplayFavourites}
					modalType={'right'}
				>
					<RightModalLayout onClose={() => this.toggleFavourites()}>
						<Collections
							collectionType={'view'}
							projectSlug={this.props.query.projectSlug}
						/>
					</RightModalLayout>
				</Modal>
			</div>
		);
	}
}

export default withRouter(Gallery);
