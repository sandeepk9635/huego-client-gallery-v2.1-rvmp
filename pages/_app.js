import React from 'react';
import App from 'next/app';

import '../src/assets/scss/global.scss';
import '../src/assets/scss/header.scss';
import '../src/assets/scss/home.scss';
import '../src/assets/scss/button.scss';
import '../src/assets/scss/modal.scss';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import '../src/assets/fonts/Inter/inter.css';
import '../src/assets/fonts/Cormorant/cormorant.css';

import Meta from '../src/views/components/header/meta';
import Service from '../src/libs/apiTunnel';

import Error404 from '../src/views/components/errorPages/404.jsx';

import _ from 'lodash';

import nookies from 'nookies';

class MyApp extends App {
	static getInitialProps = async ({ ctx }) => {
		if (
			_.has(ctx['query'], 'projectSlug') ||
			_.has(ctx['query'], 'albumID')
		) {
			const cookies = nookies.get(ctx);

			if (
				process.browser === false &&
				_.has(ctx['query'], 'projectSlug')
			) {
				let tenantID = _.has(ctx.req.headers, 'x-workspace-id')
					? ctx.req.headers['x-workspace-id']
					: 'bhogesh';

				let projectSlug = ctx['query']['projectSlug'];

				let albumUrl = `/public/${tenantID}/${projectSlug}/${ctx['query']['albumID']}`;
				let projectUrl = `/public/${tenantID}/${projectSlug}`;

				let response = await Service.fetchGet(projectUrl);

				if (response[0] === true) {
					// Set
					nookies.set(
						ctx,
						'projectData',
						JSON.stringify(response[1]),
						{
							maxAge: 30 * 24 * 60 * 60,
							path: '/',
						}
					);

					nookies.set(ctx, 'tenantID', tenantID, {
						maxAge: 30 * 24 * 60 * 60,
						path: '/',
					});

					if (_.has(ctx['query'], 'albumID')) {
						let albumResponse = await Service.fetchGet(albumUrl);
						if (albumResponse[0] === true) {
							// nookies.set(
							// 	ctx,
							// 	'albumID::' + ctx['query']['albumID'],
							// 	JSON.stringify(albumResponse[1]),
							// 	{
							// 		maxAge: 30 * 24 * 60 * 60,
							// 		path: '/',
							// 	}
							// );
							return {
								error404: false,
								projectData: response[1],
								albumData: albumResponse[1],
								query: ctx['query'],
								path: ctx['asPath'],
								pathName: ctx['pathname'],
							};
						} else {
							return {
								error404: true,
							};
						}
					} else {
						return {
							error404: false,
							projectData: response[1],
							query: ctx['query'],
							path: ctx['asPath'],
							pathName: ctx['pathname'],
						};
					}
				} else {
					return {
						error404: true,
					};
				}
			} else {
				let projectData = JSON.parse(cookies['projectData']);

				ctx = {
					...ctx,
					projectData,
				};

				if (
					ctx['pathname'] === '/[projectSlug]/[albumID]/login' ||
					ctx['pathname'] ===
						'/[projectSlug]/[albumID]/login/[loginType]'
				) {
					let tenantID = cookies['tenantID'];
					let albumUrl = `/public/${tenantID}/${projectData['slug']}/${ctx['query']['albumID']}`;
					let albumResponse = await Service.fetchGet(albumUrl);
					if (albumResponse[0] === true) {
						return {
							...ctx,
							error404: false,
							albumData: albumResponse[1],
						};
					} else {
						return {
							error404: true,
						};
					}
				} else {
					return ctx;
				}
			}
		} else {
			if (ctx.res) {
				ctx.res.writeHead(302, { Location: 'https://huego.com/' });
				ctx.res.end();
			}
			return {};
		}
	};

	render() {
		// console.log(this.props);
		if (this.props.error404) {
			return <Error404 />;
		} else {
			let {
				Component,
				pageProps,
				query,
				projectData,
				path,
				pathName,
			} = this.props;

			let updatedPageProps = {
				...pageProps,
				query,
				projectData,
				path,
				pathName,
			};

			if (_.has(this.props, 'albumData')) {
				updatedPageProps = {
					...updatedPageProps,
					albumData: this.props.albumData,
				};
			}

			return (
				<div>
					<Meta
						title={
							_.has(this.props, 'albumData')
								? this.props.albumData['title'] +
								  ' - ' +
								  this.props.albumData['galleryTitle']
								: _.has(projectData, 'title')
								? projectData['title']
								: ''
						}
						description={
							_.has(this.props, 'albumData')
								? this.props.albumData['businessName'] +
								  ' - huego'
								: _.has(projectData, 'workspaceId')
								? projectData['businessName'] + ' - huego'
								: ''
						}
						tenantId={
							_.has(projectData, 'workspaceId')
								? projectData['workspaceId']
								: ''
						}
						imageURL={
							_.has(this.props, 'albumData')
								? this.props.albumData['coverImage'][
										'coverImageUrl'
								  ]
								: _.has(projectData, 'title')
								? projectData['coverImage']['coverImageUrl']
								: ''
						}
						siteURL={
							_.has(this.props, 'albumData')
								? `https://${this.props.albumData['workspaceId']}.huego.com/${this.props.albumData['gallerySlug']}/${this.props.albumData['slug']}`
								: _.has(projectData, 'workspaceId')
								? `https://${projectData['workspaceId']}.huego.com/${projectData['slug']}`
								: ''
						}
					/>

					<Component {...updatedPageProps} />
				</div>
			);
		}
	}
}

export default MyApp;
