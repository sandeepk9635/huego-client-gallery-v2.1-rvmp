import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';
import _ from 'lodash';

var validator = require('validator');

class AuthValidation extends Component {
	validateUser = (albumGuest = false) => {
		if (!localStorage.getItem('usertoken')) {
			return this.props.router.push(
				`/${this.props.query.projectSlug}/login?from=${this.props.path}`
			);
		}
		let userToken = localStorage.getItem('usertoken');
		var decoded = jwt_decode(userToken);
		if (
			!validator.isJWT(userToken) ||
			new Date(decoded.exp * 1000) < new Date()
		) {
			// not logged in so redirect to login page with the return url
			localStorage.removeItem('usertoken');
			return this.props.router.push(
				`/${this.props.query.projectSlug}/login?from=${this.props.path}`
			);
		}

		if (
			decoded.userRole === 'guest' &&
			_.has(decoded, 'album_id') &&
			albumGuest === false
		) {
			return this.props.router.push(
				`/${this.props.query.projectSlug}/login?from=${this.props.path}`
			);
		}

		if (
			decoded.userRole === 'guest' &&
			_.has(decoded, 'album_id') &&
			albumGuest === true &&
			this.props.query.albumID !== decoded.scope
		) {
			localStorage.clear();
			return this.props.router.push(
				`/${this.props.query.projectSlug}/${this.props.query.albumID}/login`
			);
		}

		if (
			this.props.query.projectSlug !== decoded.scope &&
			decoded.userRole !== 'guest' &&
			albumGuest == false
		) {
			localStorage.clear();
			return this.props.router.push(
				`/${this.props.query.projectSlug}/login`
			);
		}
	};

	checkUserIsGuest = async () => {
		if (localStorage.getItem('usertoken')) {
			let userToken = localStorage.getItem('usertoken');
			var decoded = jwt_decode(userToken);

			if (decoded.userRole === 'guest') {
				this.setState({
					decoded,
					isGuestUser: true,
					isGuestAlbumUser: _.has(decoded, 'album_id') ? true : false,
				});
			}
		}
	};
}

export default AuthValidation;
