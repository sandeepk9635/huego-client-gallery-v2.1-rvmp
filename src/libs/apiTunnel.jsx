const env = require('./config').api_server;
const axios = require('axios');

const Service = {
	fetchGet: async (url, token = null) => {
		url = env + url;

		const headers = { 'Content-Type': 'application/json' };
		if (token) {
			headers['x-access-token'] = token;
		}

		try {
			const res = await fetch(url, { method: 'GET', headers: headers });
			const ress = await res.json();
			if (res.status >= 200 && res.status < 400) {
				return [true, ress];
			} else if (res.status === 403) {
				localStorage.clear();
				window.reload();
			} else if (res.status >= 400) {
				return [false, ress];
			} else {
				onFailure('server', url);
				return false;
			}
		} catch (e) {
			console.log(e);
			onFailure('network', url);
			return false;
		}
	},

	fetchPost: async (url, body, token = null) => {
		let URL = env + url;

		const headers = { 'Content-Type': 'application/json' };
		if (token) {
			headers['x-access-token'] = token;
		}
		try {
			const res = await fetch(URL, {
				method: 'POST',
				headers: headers,
				body: JSON.stringify(body),
			});
			const ress = await res.json();
			if (res.status >= 200 && res.status < 400) {
				return [true, ress];
			} else if (res.status >= 400) {
				return [false, ress];
			}
		} catch (e) {
			console.log(e);
			return false;
		}
	},

	fetchPut: async (url, body, token = null) => {
		let URL = env + url;

		const headers = { 'Content-Type': 'application/json' };
		if (token) {
			headers['x-access-token'] = token;
		}
		try {
			const res = await fetch(URL, {
				method: 'PUT',
				headers: headers,
				body: JSON.stringify(body),
			});
			const ress = await res.json();
			if (res.status >= 200 && res.status < 400) {
				return [true, ress];
			} else if (res.status >= 400) {
				return [res.status, ress];
			}
		} catch (e) {
			console.log(e);
			return false;
		}
	},

	fetchDelete: async (url, token = null) => {
		let URL = env + url;

		const headers = { 'Content-Type': 'application/json' };
		if (token) {
			headers['x-access-token'] = token;
		}
		try {
			const res = await fetch(URL, {
				method: 'DELETE',
				headers: headers,
			});
			const ress = await res.json();
			if (res.status >= 200 && res.status < 400) {
				return [true, ress];
			} else if (res.status >= 400) {
				return [res.status, ress];
			}
		} catch (e) {
			console.log(e);
			return false;
		}
	},

	downloadImages: async (url) => {
		url = url;

		const headers = {
			'Content-Type': 'application/json',
			responseType: 'blob',
			'Access-Control-Allow-Origin': '*',
		};

		try {
			let response = await axios({
				method: 'get',
				url: url,
				responseType: 'blob',
				headers: headers,
			});

			if (response.status === 200) {
				const imageData = response.data;
				return [true, response.data.size, imageData];
			} else {
				console.log('FAILED');
				return [false];
			}
		} catch (e) {
			return [false];
		}
	},
};

const onFailure = async (res, url) => {
	console.log('API FAILED ' + url);
	//alert(res.type);
};

export default Service;
