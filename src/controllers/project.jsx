import React, { Component } from 'react';

import * as ProjectAction from './actions';
import _ from 'lodash';

import AuthValidation from '../libs/authValidation';
import jwt_decode from 'jwt-decode';

class ProjectController extends AuthValidation {
	getProjectPublicInfo = async (projectSlug) => {
		let response = await ProjectAction.getProjectPublicInfo(projectSlug);

		if (response[0] === true) {
			this.setState({
				response: response[1],
			});
		}
	};

	getSignedURLForProject = async () => {
		if (localStorage.getItem('signedUrl')) {
			let signedUrl = JSON.parse(localStorage.getItem('signedUrl'));
			this.setState({
				signedUrl,
				isGetSignedURL: false,
			});
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getSignedURLForProject(
			this.props.projectData.slug,
			usertoken
		);

		if (response[0]) {
			localStorage.setItem('signedUrl', JSON.stringify(response[1]));
			this.setState({
				signedUrl: response[1],
				isGetSignedURL: false,
			});
		}
	};

	getSignedURLForGuestAlbum = async () => {
		if (localStorage.getItem('signedUrl')) {
			let signedUrl = JSON.parse(localStorage.getItem('signedUrl'));
			this.setState({
				signedUrl,
				isGetSignedURL: false,
			});
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getSignedURLForGuestAlbum(
			this.props.projectData.slug,
			this.props.query.albumID,
			usertoken
		);

		if (response[0]) {
			localStorage.setItem('signedUrl', JSON.stringify(response[1]));
			this.setState({
				signedUrl: response[1],
				isGetSignedURL: false,
			});
		}
	};

	getProjectDetails = async () => {
		if (localStorage.getItem('projectDetails')) {
			let projectDetails = JSON.parse(
				localStorage.getItem('projectDetails')
			);
			this.setState({
				projectDetails,
				isLoading: false,
				isRemovePin: false,
				isResetPin: false,
			});
		}
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getProjectDetails(
			this.props.projectData.slug,
			usertoken
		);

		if (response[0]) {
			localStorage.setItem('projectDetails', JSON.stringify(response[1]));

			var decoded = jwt_decode(usertoken);

			let json = {
				isPublished: true,
			};

			if (decoded.userRole === 'guest') {
				json = {
					...json,
					guestAccess: {
						isEnabled: true,
					},
				};
			}
			let projectDetailsAlbums = _.filter(response[1]['albums'], json);

			this.setState({
				projectDetails: {
					...response[1],
					albums: projectDetailsAlbums,
				},
				isLoading: false,
				isRemovePin: false,
				isResetPin: false,
			});
		}
	};

	getAlbumDetails = async (albumID) => {
		if (localStorage.getItem(albumID + '::albumDetails')) {
			let albumDetails = JSON.parse(
				localStorage.getItem(albumID + '::albumDetails')
			);

			this.setState({
				albumDetails,
				isLoading: false,
				isLoadingPrivacy: false,
			});
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getAlbumDetails(
			this.props.projectData.slug,
			albumID,
			this.state.tenantID,
			usertoken
		);

		if (response[0] === true) {
			localStorage.setItem(
				albumID + '::albumDetails',
				JSON.stringify(response[1])
			);

			this.setState({
				albumDetails: response[1],
				isLoading: false,
				isLoadingPrivacy: false,
			});
		}
	};

	getGuestAlbumDetails = async (albumID) => {
		if (localStorage.getItem(albumID + '::albumDetails')) {
			let albumDetails = JSON.parse(
				localStorage.getItem(albumID + '::albumDetails')
			);

			this.setState({
				albumDetails,
				isLoading: false,
				isLoadingPrivacy: false,
			});
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getGuestAlbumDetails(
			this.props.projectData.slug,
			albumID,
			this.state.tenantID,
			usertoken
		);

		if (response[0] === true) {
			localStorage.setItem(
				albumID + '::albumDetails',
				JSON.stringify(response[1])
			);

			this.setState({
				albumDetails: response[1],
				isLoading: false,
				isLoadingPrivacy: false,
			});
		}
	};

	getAlbumImages = async (albumID, sectionID, imageID = null) => {
		if (
			localStorage.getItem(albumID + '::' + sectionID + '::galleryImages')
		) {
			let galleryImages = JSON.parse(
				localStorage.getItem(
					albumID + '::' + sectionID + '::galleryImages'
				)
			);
			this.setGalleryImages(sectionID, galleryImages);
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getAlbumImages(
			this.props.projectData.slug,
			albumID,
			sectionID,
			usertoken
		);

		if (response[0] === true) {
			localStorage.setItem(
				albumID + '::' + sectionID + '::galleryImages',
				JSON.stringify(response[1])
			);
			this.setGalleryImages(sectionID, response[1]);

			// this  condition is for full view image.
			if (imageID != null) {
				this.setImageDetails();
			}
		}
	};

	getGuestAlbumImages = async (albumID, sectionID, imageID = null) => {
		if (
			localStorage.getItem(albumID + '::' + sectionID + '::galleryImages')
		) {
			let galleryImages = JSON.parse(
				localStorage.getItem(
					albumID + '::' + sectionID + '::galleryImages'
				)
			);
			this.setGalleryImages(sectionID, galleryImages);
		}

		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getGuestAlbumImages(
			this.props.projectData.slug,
			albumID,
			sectionID,
			usertoken
		);

		if (response[0] === true) {
			localStorage.setItem(
				albumID + '::' + sectionID + '::galleryImages',
				JSON.stringify(response[1])
			);
			this.setGalleryImages(sectionID, response[1]);

			// this  condition is for full view image.
			if (imageID != null) {
				this.setImageDetails();
			}
		}
	};

	setGalleryImages = (sectionID, galleryImagesProps) => {
		this.setState({
			galleryImages: galleryImagesProps,
			isGalleryLoading: false,
		});

		let i = 0;
		let imagesStatus = this.state.imagesStatus;
		let totalSize = 0;

		_.map(galleryImagesProps, (image, key) => {
			/*
						=====================================
								To Caluculate Size
						-------------------------------------
					*/
			// let ufff = _.filter(image.versions, {
			// 	versionId: image.activeVersionId,
			// 	isUploaded: true,
			// 	isProcessed: true,
			// });
			// ufff = ufff[0];
			// totalSize = totalSize + ufff['s3_original']['size'];
			/*
						=====================================
					*/

			imagesStatus[image['_id']] = {
				collections: 0,
				favourites: false,
				suggestedEdit: false,
				isPrivate: image.isHidden,
			};

			i++;
		});

		if (_.size(Object.keys(galleryImagesProps)) == i) {
			this.setState({
				imagesStatus,
				isImageStatusSet: false,
				imageCount: _.size(galleryImagesProps),
			});
		}
	};

	getCollections = async () => {
		if (localStorage.getItem('collections')) {
			this.setState({
				collections: JSON.parse(localStorage.getItem('collections')),
				isCollectionLoading: false,
				isAddCollectionLoading: [],
			});
		}
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getCollections(usertoken);

		if (response[0] === true) {
			localStorage.setItem('collections', JSON.stringify(response[1]));
			this.setState({
				collections: response[1],
				isCollectionLoading: false,
				isAddCollectionLoading: [],
			});
		}
	};

	getCollectionImages = async (collectionID) => {
		if (localStorage.getItem('collections::' + collectionID)) {
			let collectionImages = JSON.parse(
				localStorage.getItem('collections::' + collectionID)
			);

			this.setCollectionImages(collectionImages);
		}
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getCollectionImages(
			collectionID,
			usertoken
		);

		if (response[0] === true) {
			localStorage.setItem(
				'collections::' + collectionID,
				JSON.stringify(response[1].images)
			);
			//local
			this.setCollectionImages(response[1].images);
		}
	};

	setCollectionImages = (collectionImagesProps) => {
		this.setState({
			collectionImages: collectionImagesProps,
			isLoading: false,
			showEmptyState: _.size(collectionImagesProps) > 0 ? false : true,
			isImageStatusSet: false,
		});
	};

	createCollection = async () => {
		let usertoken = await localStorage.getItem('usertoken');
		let json = {
			title: this.state.collectionName,
		};

		let response = await ProjectAction.createCollection(json, usertoken);
		if (response[0] === true) {
			this.setState({
				isCreateCollectionLoading: false,
				collectionName: '',
			});
			this.getCollections();
			this.cancelForm(); // Function in View Favourites
		}
	};

	updateCollectionName = async (collectionID) => {
		let usertoken = await localStorage.getItem('usertoken');
		let json = {
			title: this.state.collectionName,
		};
		let response = await ProjectAction.updateCollectionName(
			json,
			collectionID,
			usertoken
		);
		if (response[0] === true) {
			this.setState({
				isCreateCollectionLoading: false,
				isDisplayInput: false,
			});
		} else {
			this.setState({
				isCreateCollectionLoading: false,
			});
		}
	};

	addImageToCollections = async (imageID, collectionID) => {
		this.setState({
			isAddCollectionLoading: [collectionID],
		});

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.addImageToCollections(
			collectionID,
			imageID,
			usertoken
		);
		if (response[0] === true) {
			this.getCollections();
		}
	};

	removeImagefromCollections = async (imageID, collectionID) => {
		this.setState({
			isAddCollectionLoading: [collectionID],
		});

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.removeImageFromCollections(
			collectionID,
			imageID,
			usertoken
		);
		if (response[0] === true) {
			this.getCollections();
		}
	};

	deleteCollection = async (collectionID) => {
		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.deleteCollection(
			collectionID,
			usertoken
		);
		if (response[0] === true) {
			this.props.handleClose();
		}
	};

	changeImagePrivacy = async (imageID, privacy) => {
		let usertoken = await localStorage.getItem('usertoken');

		let json = {
			isHidden: privacy,
		};
		let response = await ProjectAction.changeImagePrivacy(
			imageID,
			json,
			usertoken
		);

		if (response[0] === true) {
			this.getAlbumImages(
				this.props.query.albumID,
				this.props.query.galleryID,
				imageID
			);
		}
	};

	updateAlbumPrivacy = async (isEnabled, albumID = null) => {
		this.setState({
			isLoadingPrivacy: true,
		});
		let json = { isEnabled: isEnabled };

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.updateAlbumPrivacy(
			json,
			this.props.query.projectSlug,
			albumID === null ? this.props.query.albumID : albumID,
			usertoken
		);

		if (response[0] === true) {
			if (albumID === null) this.props.updateAlbumDetails();
			else this.getProjectDetails();
		}
	};

	reviewAlbum = async () => {
		let json = {
			rating: this.state.rating,
			comment: this.state.reviewText,
		};

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.postReviewForAlbum(
			json,
			this.props.projectSlug,
			this.props.albumID,
			usertoken
		);

		if (response[0]) {
			this.props.handleClose('getData');
		}
	};

	getSuggestEditForImage = async (imageID, versionID) => {
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getSuggestEditForImage(
			imageID,
			usertoken,
			versionID
		);

		if (response[0] === true) {
			this.setState({
				imageSugestions: _.size(response[1]) > 0 ? response[1][0] : {},
				isShowImageDetails: true,
				isDisplayInput: _.size(response[1]) === 0 ? true : false,
			});
		} else {
			this.setState({
				imageSugestions: [],
				isShowImageDetails: true,
				isDisplayInput: true,
			});
		}
	};

	postSuggestEditForImage = async (imageID, versionID) => {
		let json = {
			comment: this.state.suggestedEdit,
		};

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.postSuggestEditForImage(
			json,
			imageID,
			versionID,
			usertoken
		);

		if (response[0] === true) {
			this.setState({
				suggestedEdit: '',
			});
			this.getSuggestEditForImage(imageID, versionID);
		}
	};

	updateSuggesEditForImage = async (imageID, versionID, suggestedEditID) => {
		let json = {
			comment: this.state.suggestedEdit,
		};

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.updateSuggestEditForImage(
			json,
			imageID,
			versionID,
			suggestedEditID,
			usertoken
		);
		if (response[0] === true) {
			this.setState({
				suggestedEdit: '',
			});
			this.getSuggestEditForImage(imageID, versionID);
		}
	};

	deleteSuggestEditForImage = async (
		imageID,
		suggestedEditID,
		commentID,
		versionID
	) => {
		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.deleteSuggestEditForImage(
			imageID,
			suggestedEditID,
			commentID,
			usertoken
		);
		if (response[0] === true) {
			this.getSuggestEditForImage(imageID, versionID);
		}
	};

	toggleGuestProjectPin = async (pin, state) => {
		if (state === 'resetPin') {
			this.setState({
				isResetPin: true,
			});
		}

		if (state === 'removePin') {
			this.setState({
				isRemovePin: true,
			});
		}

		let json = {
			pin: pin,
		};

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.toggleGuestProjectPin(
			this.props.query.projectSlug,
			json,
			usertoken
		);

		if (response[0]) {
			if (pin)
				this.setState({
					pinShareAlbum: !this.state.pinShareAlbum,
				});
			this.getProjectShareDetails();
		}
	};

	toggleGuestAccessEnableForProject = async (isEnabled, state) => {
		let json = {
			isEnabled: isEnabled,
		};

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.toggleGuestAccessPin(
			this.props.query.projectSlug,
			json,
			usertoken
		);

		if (response[0]) {
			this.getProjectShareDetails();
		}
	};

	downloadThisImage = async () => {
		this.setState({
			isDownloadLoading: true,
		});
		let image = this.state.imageDetails;

		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.getPermissionToDownloadImage(
			image._id,
			this.state.projectDetails.canClientDownloadOriginals
				? 'original'
				: 'optimized',
			usertoken
		);

		if (response[0] === true) {
			let url = response[1].signedUrl;

			// readStream.pipe(response[1].signedUrl);

			let resp = await ProjectAction.downloadImageFromAWSS3(url);

			if (resp[0] === true) {
				this.setState({
					isDownloadLoading: false,
				});

				const link = document.createElement('a');
				link.href = window.URL.createObjectURL(resp[2]);
				link.setAttribute('download', image.displayName); //or any other extension
				document.body.appendChild(link);
				link.click();
			}
		}
	};

	getPermissionToDownloadAlbum = async (imageType = 'original') => {
		let usertoken = await localStorage.getItem('usertoken');

		let response = await ProjectAction.getPermissionToDownloadAlbum(
			this.props.query.projectSlug,
			this.props.query.albumID,
			imageType,
			usertoken
		);

		if (response[0] === true) {
			return response[1];
		}
	};

	getProjectShareDetails = async () => {
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getProjectDetailsShare(
			this.props.projectData.slug,
			usertoken
		);

		if (response[0]) {
			this.setState({
				projectDetailsShare: response[1],
				isProjectDetailsShareLoading: false,
				isRemovePin: false,
				isResetPin: false,
			});
		}
	};

	getAlbumShareDetails = async () => {
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.getAlbumDetailsShare(
			this.props.projectData.slug,
			this.props.query.albumID,
			usertoken
		);

		if (response[0]) {
			this.setState({
				projectDetailsShare: response[1],
				isProjectDetailsShareLoading: false,
				isRemovePin: false,
				isResetPin: false,
			});
		}
	};

	toggleGuestAlbumPin = async (pin) => {
		let json = {
			pin: pin,
		};
		let usertoken = await localStorage.getItem('usertoken');
		let response = await ProjectAction.updateAlbumShare(
			this.props.projectData.slug,
			this.props.query.albumID,
			json,
			usertoken
		);

		if (response[0]) {
			this.getAlbumShareDetails();
		}
	};
}

export default ProjectController;
