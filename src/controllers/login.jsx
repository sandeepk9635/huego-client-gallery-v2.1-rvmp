import React, { Component } from 'react';

import * as LoginAction from './actions';
import _ from 'lodash';
import ProjectController from './project';

class LoginController extends ProjectController {
	verifyProjectGuestPin = async (from) => {
		let json = {};
		if (this.props.projectData.guestAccess.pinRequired === false) {
			json = {
				workspaceId: this.props.projectData.workspaceId,
			};
		} else {
			json = {
				guestAccessPin: this.state.otp,
				workspaceId: this.state.tenantID,
			};
		}

		let response = await LoginAction.verifyProjectGuestPin(
			this.props.query.projectSlug,
			json
		);

		if (response[0] === true) {
			localStorage.setItem('usertoken', response[1].accessToken);

			if (from === '') {
				this.props.router.push('/' + this.props.query.projectSlug);
			} else {
				this.props.router.push(from);
			}
		} else {
			this.setState({
				errorMessage: response[1].message,
				isOTPDisabled: false,
				isLoading: false,
				otp: '',
			});
		}
	};

	verifyProjectMasterPin = async (from) => {
		let json = {
			masterAccessPin: this.state.otp,
			workspaceId: this.state.tenantID,
		};

		let response = await LoginAction.verifyProjectMasterPin(
			this.props.query.projectSlug,
			json
		);

		if (response[0] === true) {
			localStorage.setItem('usertoken', response[1].accessToken);

			if (from === '') {
				this.props.router.push('/' + this.props.query.projectSlug);
			} else {
				this.props.router.push(from);
			}
		} else {
			this.setState({
				errorMessage: response[1].message,
				isOTPDisabled: false,
				isLoading: false,
				otp: '',
			});
		}
	};

	verifyAlbumLoginPin = async (from) => {
		let json = {};
		if (this.props.albumData.guestAccess.pinRequired === false) {
			json = {
				workspaceId: this.props.projectData.workspaceId,
			};
		} else {
			json = {
				guestAccessPin: this.state.otp,
				workspaceId: this.state.tenantID,
			};
		}

		let response = await LoginAction.verifyAlbumLogin(
			this.props.query.projectSlug,
			this.props.query.albumID,
			json
		);

		if (response[0] === true) {
			localStorage.setItem('usertoken', response[1].accessToken);

			this.props.router.push(
				'/' +
					this.props.query.projectSlug +
					'/' +
					this.props.query.albumID +
					'/' +
					response[1]['albumInfo'].sections[0]['_id']
			);
		} else {
			this.setState({
				errorMessage: response[1].message,
				isOTPDisabled: false,
				isLoading: false,
				otp: '',
			});
		}
	};
}

export default LoginController;
