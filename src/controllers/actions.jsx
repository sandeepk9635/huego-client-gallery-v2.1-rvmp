import * as API from './actionTypes';

import service from '../libs/apiTunnel';

/*

    -----------------------------------------------------
                Public API's to Get Info
    -----------------------------------------------------
    

*/

export const getProjectPublicInfo = async (projectID) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY_LOGIN.url +
			projectID +
			API.END_USERS_GALLERY_LOGIN.status
	);
};

/*

    -------------------------------------
                Login Actions
    -------------------------------------
    
*/

export const verifyProjectMasterPin = async (projectID, payload) => {
	return await service.fetchPost(
		API.END_USERS_GALLERY_LOGIN.url +
			projectID +
			API.END_USERS_GALLERY_LOGIN.verifyMaster,
		payload
	);
};

export const verifyProjectGuestPin = async (projectID, payload) => {
	return await service.fetchPost(
		API.END_USERS_GALLERY_LOGIN.url +
			projectID +
			API.END_USERS_GALLERY_LOGIN.verifyGuest,
		payload
	);
};

export const verifyAlbumLogin = async (projectID, albumID, payload) => {
	return await service.fetchPost(
		API.END_USERS_GALLERY_LOGIN.url +
			projectID +
			API.END_USERS_GALLERY_LOGIN.albums +
			albumID,
		payload
	);
};

/*

    -------------------------------------
                Signed URL
    -------------------------------------
    
*/

export const getSignedURLForProject = async (projectID, usertoken) => {
	return await service.fetchGet(
		API.GET_PRESIGNED_URL.get_presigned_url + '/' + projectID,
		usertoken
	);
};

export const getSignedURLForGuestAlbum = async (
	projectID,
	albumID,
	usertoken
) => {
	return await service.fetchGet(
		API.GET_PRESIGNED_URL.get_presigned_url_for_guest +
			'/' +
			projectID +
			'/' +
			albumID,
		usertoken
	);
};

/*

    -----------------------------------------------
                Project Home Actions
    -----------------------------------------------
    
*/

export const getProjectDetails = async (projectID, usertoken) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url + '/' + projectID,
		usertoken
	);
};

/*

    -----------------------------------------------
                Album Home Actions
    -----------------------------------------------
    
*/

export const getAlbumDetails = async (
	projectID,
	albumID,
	tenantID,
	usertoken
) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID,
		usertoken
	);
};

export const getAlbumImages = async (
	projectID,
	albumID,
	sectionID,
	usertoken
) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.END_USERS_GALLERY.sections +
			sectionID +
			API.END_USERS_GALLERY.images,
		usertoken
	);
};

export const updateAlbumPrivacy = async (
	payload,
	projectID,
	albumID,
	usertoken
) => {
	return await service.fetchPut(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.END_USERS_GALLERY.guestAccess,
		payload,
		usertoken
	);
};

export const postReviewForAlbum = async (
	json,
	projectSlug,
	albumID,
	usertoken
) => {
	return await service.fetchPut(
		API.END_USERS_GALLERY.url +
			'/' +
			projectSlug +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.END_USERS_GALLERY.review,
		json,
		usertoken
	);
};

/*

    -----------------------------------------------
                Collection Actions
    -----------------------------------------------
    
*/

export const getCollections = async (usertoken) => {
	return await service.fetchGet(
		API.END_USERS_COLLECTION.collections,
		usertoken
	);
};

export const getCollectionImages = async (collectionID, usertoken) => {
	return await service.fetchGet(
		API.END_USERS_COLLECTION.collections +
			'/' +
			collectionID +
			API.END_USERS_COLLECTION.addImage,
		usertoken
	);
};

export const createCollection = async (payload, token) => {
	return await service.fetchPost(
		API.END_USERS_COLLECTION.collections,
		payload,
		token
	);
};

export const addImageToCollections = async (
	collectionID,
	imageID,
	usertoken
) => {
	return await service.fetchPost(
		API.END_USERS_COLLECTION.collections +
			'/' +
			collectionID +
			API.END_USERS_COLLECTION.addImage +
			'/' +
			imageID,
		{},
		usertoken
	);
};

export const removeImageFromCollections = async (
	collectionID,
	imageID,
	usertoken
) => {
	return await service.fetchDelete(
		API.END_USERS_COLLECTION.collections +
			'/' +
			collectionID +
			API.END_USERS_COLLECTION.addImage +
			'/' +
			imageID,
		usertoken
	);
};

export const updateCollectionName = async (payload, collectionID, token) => {
	return await service.fetchPut(
		API.END_USERS_COLLECTION.collections + '/' + collectionID,
		payload,
		token
	);
};

export const deleteCollection = async (collectionID, token) => {
	return await service.fetchDelete(
		API.END_USERS_COLLECTION.collections + '/' + collectionID,
		token
	);
};

/*

    -----------------------------------------------
                Image Actions
    -----------------------------------------------
    
*/

export const changeImagePrivacy = async (imageID, json, usertoken) => {
	return await service.fetchPut(
		API.GALLERY_IMAGES.url + '/' + imageID + API.GALLERY_IMAGES.privacy,
		json,
		usertoken
	);
};

export const getSuggestEditForImage = async (imageID, usertoken, versionID) => {
	return await service.fetchGet(
		API.GALLERY_IMAGES.url +
			'/' +
			imageID +
			API.GALLERY_IMAGES.suggestEdit +
			API.GALLERY_IMAGES.versions +
			versionID,
		usertoken
	);
};

export const postSuggestEditForImage = async (
	json,
	imageID,
	versionID,
	usertoken
) => {
	return await service.fetchPost(
		API.GALLERY_IMAGES.url +
			'/' +
			imageID +
			API.GALLERY_IMAGES.suggestEdit +
			API.GALLERY_IMAGES.versions +
			versionID,
		json,
		usertoken
	);
};

export const updateSuggestEditForImage = async (
	json,
	imageID,
	versionID,
	suggestedEditID,
	usertoken
) => {
	return await service.fetchPost(
		API.GALLERY_IMAGES.url +
			'/' +
			imageID +
			API.GALLERY_IMAGES.suggestEdit +
			'/' +
			suggestedEditID +
			API.GALLERY_IMAGES.versions +
			versionID,
		json,
		usertoken
	);
};

export const deleteSuggestEditForImage = async (
	imageID,
	suggestedEditID,
	commentID,
	usertoken
) => {
	return await service.fetchDelete(
		API.GALLERY_IMAGES.url +
			'/' +
			imageID +
			API.GALLERY_IMAGES.suggestEdit +
			'/' +
			suggestedEditID +
			'/' +
			commentID,
		usertoken
	);
};

export const toggleGuestProjectPin = async (projectID, payload, token) => {
	return await service.fetchPut(
		API.GALLERY_SHARING.projects +
			projectID +
			API.GALLERY_SHARING.changeGuestAccess,
		payload,
		token
	);
};

export const toggleGuestAccessPin = async (projectID, payload, token) => {
	return await service.fetchPut(
		API.GALLERY_SHARING.projects +
			projectID +
			API.GALLERY_SHARING.changeGuestAccess,
		payload,
		token
	);
};

export const getPermissionToDownloadImage = async (
	imageID,
	downloadType,
	usertoken
) => {
	return await service.fetchGet(
		API.GALLERY_IMAGES.url +
			'/' +
			imageID +
			API.GALLERY_IMAGES.download +
			API.GALLERY_IMAGES.downloadType +
			downloadType,
		usertoken
	);
};

export const downloadImageFromAWSS3 = async (url) => {
	return await service.downloadImages(url);
};

export const getPermissionToDownloadAlbum = async (
	projectID,
	albumID,
	downloadType,
	usertoken
) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.GALLERY_IMAGES.download +
			API.GALLERY_IMAGES.downloadType +
			downloadType,
		usertoken
	);
};

export const getProjectDetailsShare = async (projectID, usertoken) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.shareDetails,
		usertoken
	);
};

export const getAlbumDetailsShare = async (projectID, albumID, usertoken) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.END_USERS_GALLERY.changeGuestAccess,
		usertoken
	);
};

export const updateAlbumShare = async (
	projectID,
	albumID,
	payload,
	usertoken
) => {
	return await service.fetchPut(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.albums +
			albumID +
			API.END_USERS_GALLERY.changeGuestAccess,
		payload,
		usertoken
	);
};

/*

    -----------------------------------------------
                Get Album Guest Actions
    -----------------------------------------------
    
*/
export const getGuestAlbumImages = async (
	projectID,
	albumID,
	sectionID,
	usertoken
) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.guestAlbums +
			albumID +
			API.END_USERS_GALLERY.sections +
			sectionID +
			API.END_USERS_GALLERY.images,
		usertoken
	);
};

export const getGuestAlbumDetails = async (
	projectID,
	albumID,
	tenantID,
	usertoken
) => {
	return await service.fetchGet(
		API.END_USERS_GALLERY.url +
			'/' +
			projectID +
			API.END_USERS_GALLERY.guestAlbums +
			albumID,
		usertoken
	);
};
