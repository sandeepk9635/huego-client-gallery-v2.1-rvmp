export const END_USERS_GALLERY_LOGIN = {
	url: '/login/galleries/',
	status: '/status',
	verifyMaster: '/master',
	verifyGuest: '/guest',
	albums: '/albums/',
};

export const END_USERS_GALLERY = {
	url: '/galleries',
	tenantID: '?workspaceId=',
	accessID: '&accessID=',
	albums: '/albums/',
	images: '/images',
	sections: '/sections/',
	imageprivacy: '/image-privacy',
	isPrivate: '&isPrivate=',
	guestAccess: '/guest-access',
	isEnabled: '&isEnabled=',
	review: '/master-review',
	suggestEdit: '/suggest-edit',
	imageParams: '&image_id=',
	versionId: '&versionId=',
	delete: '/delete',
	download: '/download',
	downloadType: '&type=',
	projects: '/projects/',
	addComment: '/add-comment',
	shareDetails: '/share-details',
	changeGuestAccess: '/guest-access',
	guestAlbums: '/guest-albums/',
};

export const GET_PRESIGNED_URL = {
	get_presigned_url: '/content-distribution/get-credentials',
	get_presigned_url_for_guest:
		'/content-distribution/guest-album-credentials',
	tenantID: '?workspaceId=',
};

export const END_USERS_COLLECTION = {
	endUsers: '/end-users/',
	collections: '/gallery-collections',
	tenantID: '?workspaceId=',
	addImage: '/images',
	removeImage: '/remove-image',
	rename: '/re-name',
	delete: '/delete',
};

export const GALLERY_IMAGES = {
	url: '/gallery-images',
	suggestEdit: '/suggested-edits',
	versions: '?versionId=',
	privacy: '/privacy',
	isHidden: '?isHidden=',
	download: '/download',
	downloadType: '?imageType=',
};

export const GALLERY_SHARING = {
	projects: '/galleries/',
	changeGuestAccess: '/guest-access',
	toggleGuestAccess: '/toggle-guest-access',
	pin: '?pin=',
	isEnabled: '?isEnabled=',
};
