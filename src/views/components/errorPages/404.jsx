import React, { Component } from 'react';
import Image from 'next/image';
class Error404 extends Component {
	render() {
		return (
			<div className="error-page-center ">
				<div className="w-100p h-100p f-left d-flex align-center">
					<div className="content">
						<div className="img w-100p h-100p f-left">
							<Image
								src="/error404.png"
								alt="404"
								width="300"
								height="300"
							/>
						</div>
						<div className="text-t w-100p f-left text-center">
							Page Not Found
						</div>
						<div className="text-b w-100p f-left text-center">
							Sorry, we couldn’t find the page you’re looking for.
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Error404;
