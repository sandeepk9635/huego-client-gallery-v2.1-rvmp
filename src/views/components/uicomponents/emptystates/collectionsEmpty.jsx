import React, { Component } from 'react';
import Favlarge from '../../../../assets/svg/favlarge.svg';
import Nophotos from '../../../../assets/svg/nophotos.svg';
class collectionsEmpty extends Component {
	render() {
		return (
			<div className="empty-state-wrapper d-flex align-center">
				<div className="empty-state-sub-wrapper">
					<div className="icon-wrapper f-left w-100p">
						<div className="icon-sub-wrapper ">
							<div className="icon-container d-flex align-center">
								{this.props.emptyalbum === true ? (
									<Nophotos />
								) : (
									<Favlarge />
								)}
							</div>
						</div>
					</div>
					<div className="title w-100p f-left">
						No Photos in this collection
					</div>
					<div className="desc w-100p f-left">
						Click on the heart icon to add coleections
					</div>
				</div>
			</div>
		);
	}
}

export default collectionsEmpty;
