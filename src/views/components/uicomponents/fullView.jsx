import React, { Component } from 'react';

import Header from '../../../views/components/header/header';
import Download from '../../../assets/svg/download.svg';
import Fav from '../../../assets/svg/fav.svg';
import Unlock from '../../../assets/svg/unlock.svg';
import Versions from '../../../assets/svg/versions.svg';
import Slider from 'react-slick';

import { withRouter } from 'next/router';

class Home extends Component {
	constructor() {
		super();
		this.state = {
			settings: {
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
			},
			showActionbar: false,
		};
	}
	toggleActionbar = () => {
		document.body.style.overflow = 'hidden';
		this.setState({
			showActionbar: !this.state.showActionbar,
		});
	};

	render() {
		var settings = {
			...this.state.settings,
			afterChange: (current) =>
				this.setState({
					showActionbar: false,
				}),
		};
		return (
			<div className="wrapper">
				<div className="container">
					<div className="image-container w-100p">
						<Slider {...settings}>
							{/* Slide One */}

							<div className="image-wrapper">
								<img
									onClick={() => this.toggleActionbar()}
									src={
										'https://images.ctfassets.net/2onq0fbdrig0/xxSW01bP41Vkcphktjqa0/99fd04355525a488c2572855cd2a544b/AnishAlankritaPhotos1.jpg?w=1600&h=2298&q=50'
									}
								/>
							</div>

							{/* SLide Two */}
							<div className="image-wrapper">
								<img
									onClick={() => this.toggleActionbar()}
									src={
										'https://images.ctfassets.net/2onq0fbdrig0/6r1XiRsvNhlciz90hROXWH/3c11af1b4d74212361ead98dc47dc1e0/AnishAlankritaPhotos6.jpg?w=1600&h=1068&q=50'
									}
								/>
							</div>
							{/* SLide Three */}
							<div className="image-wrapper">
								<img
									onClick={() => this.toggleActionbar()}
									src={
										'https://images.unsplash.com/photo-1486074051793-e41332bf18fc?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1000&q=80'
									}
								/>
							</div>
						</Slider>
					</div>
					<div
						className={
							this.state.showActionbar
								? 'header-versions-wrapper slidetopDown'
								: 'header-versions-wrapper slidetopUp'
						}
					>
						<Header
							screenName={'fullView'}
							onClick={() => this.props.onBackButtonClick()}
						/>
						<div className="versions-wrapper w-100p d-flex align-center">
							<div className="icon-with-text">
								<a className="f-left">
									<Versions />
								</a>
								<span className="f-left">v2</span>
							</div>
						</div>
					</div>
					<div
						className={
							this.state.showActionbar
								? 'thumbnails-actionbar-wrapper slidebottomUp'
								: 'thumbnails-actionbar-wrapper slidebottomDown'
						}
					>
						<div className="thumbnails-wrapper w-100p"></div>
						<div className="action-bar w-100p">
							<a>
								<Download />
							</a>
							<a>
								<Fav />
							</a>
							<a>
								<Unlock />
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(Home);
