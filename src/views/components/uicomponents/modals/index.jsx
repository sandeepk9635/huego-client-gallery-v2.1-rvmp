import React, { Component } from 'react';
import Close from '../../../../assets/svg/close.svg';
import { Modal } from 'react-bootstrap';

class modal extends Component {
	constructor() {
		super();
		this.state = {
			slideclassright: 'sliding-right-hide',
			openclasscenter: 'sliding-center-hide',
			slideclassbottom: 'sliding-bottom-hide',
		};
	}
	modalswitch = (e) => {
		switch (e) {
			case 'right':
				return 'rightModal';
			case 'center':
				return 'centerModal';

			case 'bottom':
				return 'bottomModal';

			default:
				return 'centerModal';
		}
	};
	modalcloseswitch = (e) => {
		switch (e) {
			case 'right':
				return (
					<a
						className="m-close d-flex align-center"
						onClick={() => this.props.handleClose()}
					>
						<Close />
					</a>
				);
			case 'center':
				return <span></span>;

			case 'bottom':
				return (
					<a
						className="m-close d-flex align-center"
						onClick={() => this.props.handleClose()}
					>
						<Close />
					</a>
				);

			default:
				return <span></span>;
		}
	};
	enteringclass = () => {
		if (this.props.modalType === 'right') {
			this.setState({
				slideclassright: 'sliding-right-show',
			});
		}
		if (this.props.modalType === 'center') {
			this.setState({
				openclasscenter: 'sliding-center-show',
			});
		}
		if (this.props.modalType === 'bottom') {
			this.setState({
				slideclassbottom: 'sliding-bottom-show',
			});
		}
	};
	exitingclass = () => {
		if (this.props.modalType === 'right') {
			this.setState({
				slideclassright: 'sliding-right-hide',
			});
		}
		if (this.props.modalType === 'center') {
			this.setState({
				openclasscenter: 'sliding-center-hide',
			});
		}
		if (this.props.modalType === 'bottom') {
			this.setState({
				slideclassbottom: 'sliding-bottom-hide',
			});
		}
	};
	modalanimationswitch = (e) => {
		switch (e) {
			case 'right':
				return this.state.slideclassright;

			case 'center':
				return this.state.openclasscenter;
			case 'bottom':
				return this.state.slideclassbottom;
			default:
				return '';
		}
	};
	render() {
		return (
			<Modal
				show={this.props.show}
				onHide={() => this.props.handleClose()}
				keyboard={true}
				className={this.modalswitch(this.props.modalType)}
				onEntering={this.enteringclass}
				onExiting={this.exitingclass}
				backdrop={true}
			>
				<Modal.Body
					scrollable={true}
					id={this.modalanimationswitch(this.props.modalType)}
				>
					<div className={'m-body'}>{this.props.children}</div>
				</Modal.Body>
			</Modal>
		);
	}
}

export default modal;
