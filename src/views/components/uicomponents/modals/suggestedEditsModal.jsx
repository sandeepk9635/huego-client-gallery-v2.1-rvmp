import React, { Component } from 'react';
import Versionicon from '../../../../assets/svg/versionicon.svg';
import Closeicon from '../../../../assets/svg/close.svg';
import Submiticon from '../../../../assets/svg/submit.svg';
import SuggesteditItem from '../suggesteditItem.jsx';

import Input from '../../global/input';

import Loader from '../../../components/global/loader';
import ProjectController from '../../../../controllers/project';

const moment = require('moment');

class bottomModal extends ProjectController {
	constructor() {
		super();
		this.state = {
			isShowImageDetails: false,
			versionImageDetails: {},
			imageSugestions: [],
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		_.map(this.props.imageDetails.versions, (imageVersion, key) => {
			if (
				imageVersion.versionId ===
				this.props.imageDetails.activeVersionId
			) {
				this.setState({
					initialSlide: key,
					currentSlide: key + 1,
					versionImageDetails: this.props.imageDetails.versions[key],
				});

				this.getSuggestEditForImage(
					this.props.imageDetails._id,
					this.props.imageDetails.versions[key]['versionId']
				);
			}
		});
	};

	saveSuggestEditInput = (e) => {
		this.setState({
			errorMessage: '',
			suggestedEdit: e.target.value,
		});
	};

	render() {
		return (
			<div className="suggested-edits">
				<div className="m-header w-100p f-left">
					<div
						className="header-left d-flex align-center"
						style={{ width: this.props.hleftwidth }}
					>
						<div className={'header-item f-left icon-with-text'}>
							<div className="icon">
								<Versionicon />
							</div>
							<div className="text">
								v
								{this.state.currentSlide < 9
									? '0' + this.state.currentSlide
									: this.state.currentSlide}
							</div>
						</div>
					</div>
					<div
						className="header-center d-flex align-center"
						style={{ width: this.props.hcenterwidth }}
					>
						<div className="center-text-holder">
							<div className="file-name w-100p f-left text-center">
								Suggest an edit
							</div>
							<div className="date-time w-100p f-left text-center">
								{this.props.imageDetails.displayName}
							</div>
						</div>
					</div>
					<div
						className="header-right d-flex align-center"
						style={{ width: this.props.hrightwidth }}
					>
						<a
							className="header-item main-menu-icon fav-icon"
							onClick={() => this.props.handleClose()}
						>
							<Closeicon />
						</a>
					</div>
				</div>
				<div className="m-body w-100p f-left">
					{this.state.isShowImageDetails ? (
						<React.Fragment>
							{_.map(
								this.state.imageSugestions.suggestions,
								(imageSuggestion) => {
									return (
										<SuggesteditItem
											data={imageSuggestion}
											versionKey={this.state.currentSlide}
											deleteSuggestEditForImage={(
												commentID
											) =>
												this.deleteSuggestEditForImage(
													this.props.imageDetails._id,
													this.state.imageSugestions
														._id,
													commentID,
													this.props.imageDetails
														.activeVersionId
												)
											}
										/>
									);
								}
							)}
						</React.Fragment>
					) : (
						<Loader type={'image'} />
					)}
				</div>

				<React.Fragment>
					<p className="error-message">{this.state.errorMessage}</p>
					<div className="m-footer w-100p f-left d-flex align-center">
						<div className="footer-wrapper">
							<div className="input-wrapper">
								<Input
									classname={'form-input'}
									type={'textarea'}
									rows={2}
									value={this.state.suggestedEdit}
									onChange={(e) =>
										this.saveSuggestEditInput(e)
									}
								/>
							</div>

							<div
								className="button-wrapper"
								onClick={() =>
									_.size(this.state.imageSugestions) > 0
										? this.updateSuggesEditForImage(
												this.props.imageDetails._id,
												this.props.imageDetails
													.activeVersionId,
												this.state.imageSugestions[
													'_id'
												]
										  )
										: this.postSuggestEditForImage(
												this.props.imageDetails._id,
												this.props.imageDetails
													.activeVersionId
										  )
								}
							>
								<Submiticon />
							</div>
						</div>
					</div>
				</React.Fragment>
			</div>
		);
	}
}

export default bottomModal;
