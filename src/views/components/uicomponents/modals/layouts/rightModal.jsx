import React, { Component } from 'react';
import Closeicon from '../../../../../assets/svg/close.svg';
import Favred from '../../../../../assets/svg/favred.svg';
class rightModal extends Component {
	render() {
		return (
			<div className={`left-modal-layout left-modal-layout-display `}>
				<div className="lm-header w-100p f-left">
					<div className="lm-header-wrapper d-flex align-center">
						<div className="left-d w-50p f-left">
							<div className="icon-with-text">
								<div className="icon f-left">
									<Favred />
								</div>
								<div className="text f-left">Favourites</div>
							</div>
						</div>
						<div className="right-d w-50p f-left d-flex align-center">
							<div
								className="close-icon"
								onClick={() => this.props.onClose()}
							>
								<Closeicon />
							</div>
						</div>
					</div>
				</div>
				<div className="lm-body w-100p f-left">
					{this.props.children}
				</div>
			</div>
		);
	}
}

export default rightModal;
