import React, { Component } from 'react';

class modalLayout extends Component {
	render() {
		return (
			<div
				className={
					this.props.type === 'bottom'
						? 'bottom-modal w-100p '
						: 'center-modal w-100p '
				}
			>
				{this.props.children}
			</div>
		);
	}
}

export default modalLayout;
