import React, { Component } from 'react';
import Closeicon from '../../../../assets/svg/close.svg';
import Button from '../../../components/global/button';
import ProjectController from '../../../../controllers/project';

class deleteModal extends ProjectController {
	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
	};

	render() {
		return (
			<div className="delete-modal w-100p f-left">
				<div className="c-header w-100p f-left d-flex align-center">
					<Closeicon onClick={() => this.props.handleClose()} />
				</div>
				<div className="c-body w-100p f-left d-flex align-center">
					<div className="w-100p f-left c-wrap ">
						<div className="c-text w-100p f-left text-center">
							Are you sure you want to delete this collection?
						</div>
						<div className="c-button w-100p f-left">
							<div className="c-buttonwrapper">
								<div className="c-buttonwrap w-50p f-left">
									<Button
										type={'tertiary'}
										danger={true}
										name={'Delete'}
										onClick={() =>
											this.deleteCollection(
												this.props.collectionID
											)
										}
									/>
								</div>
								<div className="c-buttonwrap w-50p f-left">
									<Button
										type={'tertiary'}
										danger={false}
										name={'Cancel'}
										onClick={() => this.props.handleClose()}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default deleteModal;
