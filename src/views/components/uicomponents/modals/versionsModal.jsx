import React, { Component } from 'react';
import Versionicon from '../../../../assets/svg/versionicon.svg';
import Closeicon from '../../../../assets/svg/close.svg';
import VersionItem from '../suggesteditItem.jsx';
import Slider from 'react-slick';

import Loader from '../../../components/global/loader';
import ProjectController from '../../../../controllers/project';

import RightArrow from '../../../../assets/svg/rightarrow.svg';
import LeftArrow from '../../../../assets/svg/leftarrow.svg';
const moment = require('moment');

class versionsModal extends ProjectController {
	constructor() {
		super();
		this.state = {
			settings: {
				className: 'center',
				centerMode: true,
				infinite: true,
				centerPadding: '60px',
				slidesToShow: 1,
				speed: 500,
				nextArrow: <RightArrow />,
				prevArrow: <LeftArrow />,
				dots: false,
			},
			showArrows: false,
			isShowImageDetails: false,
			versionImageDetails: {},
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		_.map(this.props.imageDetails.versions, (imageVersion, key) => {
			if (
				imageVersion.versionId ===
				this.props.imageDetails.activeVersionId
			) {
				this.setState({
					initialSlide: key,
					currentSlide: key + 1,
					versionImageDetails: this.props.imageDetails.versions[key],
				});

				this.beforeSlideChange(key);
			}
		});
		if (window.innerWidth >= 768) {
			this.setState({
				showArrows: true,
			});
		}
	};

	beforeSlideChange = (slideKey) => {
		this.setState({
			currentSlide: slideKey + 1,
			versionImageDetails: this.props.imageDetails.versions[slideKey],
			isShowImageDetails: false,
		});

		this.getSuggestEditForImage(
			this.props.imageDetails._id,
			this.props.imageDetails.versions[slideKey]['versionId']
		);
	};

	renderVersionSlideShow = () => {
		return _.map(this.props.imageDetails.versions, (imageVersion, key) => {
			if (imageVersion.givenFileName != null) {
				let url =
					this.props.signedUrl['baseUrl'] +
					'optimized/' +
					imageVersion.givenFileName +
					'?Key-Pair-Id=' +
					this.props.signedUrl.credentials['Key-Pair-Id'] +
					'&Policy=' +
					this.props.signedUrl.credentials['Policy'] +
					'&Signature=' +
					this.props.signedUrl.credentials['Signature'];

				return (
					<div className="img-container">
						<img
							src={url}
							alt={`Version ${key + 1} ${
								this.props.imageDetails.displayName
							}`}
						/>
						<div className="gradient-overlay"></div>
						<div className="info-overlay">
							<a className="d-flex align-center f-left">
								<Versionicon />v{key + 1}
							</a>
							{imageVersion.versionId ===
							this.props.imageDetails.activeVersionId ? (
								<span className={'d-flex align-center'}>
									current
								</span>
							) : (
								''
							)}
						</div>
					</div>
				);
			}
		});
	};
	render() {
		var settings = {
			...this.state.settings,
			initialSlide: this.state.initialSlide,
			beforeChange: (current, next) => this.beforeSlideChange(next),
		};

		return (
			<div className="suggested-edits" style={{ height: '100%' }}>
				<div className="m-header w-100p f-left">
					<div className="header-left d-flex align-center">
						<div className={'header-item f-left icon-with-text'}>
							<div className="icon">
								<Versionicon />
							</div>
							<div className="text">
								v{_.size(this.props.imageDetails.versions)}
							</div>
						</div>
					</div>
					<div className="header-center d-flex align-center">
						<div className="center-text-holder">
							<div className="file-name w-100p f-left text-center">
								Multiple Versions
							</div>
							<div className="date-time w-100p f-left text-center">
								{this.props.imageDetails.displayName}
							</div>
						</div>
					</div>
					<div className="header-right d-flex align-center">
						<a
							className="header-item main-menu-icon fav-icon"
							onClick={() => this.props.handleClose()}
						>
							<Closeicon />
						</a>
					</div>
				</div>
				<div
					className="m-body w-100p f-left"
					style={{ height: '100%' }}
				>
					<div className="image-wrapper w-100p f-left">
						<Slider {...settings} arrows={this.state.showArrows}>
							{this.renderVersionSlideShow()}
						</Slider>
					</div>

					<div className="version-info-wrapper">
						<div className="w-100p f-left version-info-item">
							<div className="w-50p f-left title">Version</div>
							<div className="w-50p f-left desc">
								{this.state.currentSlide < 9
									? '0' + this.state.currentSlide
									: this.state.currentSlide}
							</div>
						</div>
						<div className="w-100p f-left version-info-item">
							<div className="w-50p f-left title">
								Uploaded On
							</div>

							<div className="w-50p f-left desc">
								{moment(
									parseInt(
										this.state.versionImageDetails.versionId
									)
								).format('DD MMM YYYY')}
							</div>
						</div>
					</div>
					<div
						className={'w-100p'}
						style={{ height: '200px', overflowY: 'auto' }}
					>
						{this.state.isShowImageDetails ? (
							<React.Fragment>
								{_.size(this.state.imageSugestions) > 0
									? _.map(
											this.state.imageSugestions
												.suggestions,
											(suggestion) => {
												console.log(suggestion);
												return (
													<VersionItem
														type={'version'}
														versionKey={
															this.state
																.currentSlide
														}
														data={suggestion}
														//editComment={() => this.editSuggestion()}
														deleteSuggestEditForImage={(
															commentID
														) =>
															this.deleteSuggestEditForImage(
																this.props
																	.imageDetails
																	._id,
																this.state
																	.imageSugestions
																	._id,
																commentID,
																this.state
																	.versionImageDetails
																	.versionId
															)
														}
													/>
												);
											}
									  )
									: ''}
								;
							</React.Fragment>
						) : (
							<Loader type={'image'} />
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default versionsModal;
