import React, { Component } from 'react';
import Versionicon from '../../../../assets/svg/versionicon.svg';
import Closeicon from '../../../../assets/svg/close.svg';
import Submiticon from '../../../../assets/svg/submit.svg';
import SuggesteditItem from '../suggesteditItem.jsx';
import Button from '../../global/button';

import Input from '../../global/input';

import Rating from '../../../../assets/svg/rating.svg';
import Ratingfilled from '../../../../assets/svg/ratingfilled.svg';

import ProjectController from '../../../../controllers/project';
import _ from 'lodash';

class bottomModal extends ProjectController {
	constructor() {
		super();
		this.state = {
			rating: -1,

			rating_0: false,
			rating_1: false,
			rating_2: false,
			rating_3: false,
			rating_4: false,

			reviewText: '',
			errorMessage: '',

			reviewErrorMessage: '',
			isReviewInputError: false,
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});

		if (_.size(this.props.reviewData) > 0) {
			this.setState({
				rating: this.props.reviewData[_.size(this.props.reviewData) - 1]
					.rating,
				reviewText: this.props.reviewData[
					_.size(this.props.reviewData) - 1
				].comment,
			});
		}
	};

	handleClickPlus = (rating) => {
		this.setState({ rating });
	};

	validateReviewForm = () => {
		if (this.state.rating === -1) {
			this.setState({
				errorMessage: 'Rating is Mandatory',
			});
		}

		if (this.state.reviewText === null || this.state.reviewText === '') {
			this.setState({
				isReviewInputError: true,
				reviewErrorMessage: 'Required',
			});
		}

		if (
			this.state.reviewText !== null &&
			this.state.reviewText !== '' &&
			this.state.rating != -1
		) {
			this.reviewAlbum();
		}
	};

	saveReviewInput = (e) => {
		this.setState({
			reviewText: e.target.value,
			isReviewInputError: false,
			reviewErrorMessage: '',
		});
	};

	render() {
		return (
			<div className="suggested-edits share-feedback">
				<div className="m-header w-100p f-left">
					<div className="header-left d-flex align-center"></div>
					<div className="header-center d-flex align-center">
						<div className="center-text-holder">
							<div className="file-name w-100p f-left text-center">
								Share Your Feedback
							</div>
							<div className="date-time w-100p f-left text-center">
								Engagement
							</div>
						</div>
					</div>
					<div className="header-right d-flex align-center">
						<a
							className="header-item main-menu-icon fav-icon"
							onClick={() => this.props.handleClose()}
						>
							<Closeicon />
						</a>
					</div>
				</div>
				<div className="m-body w-100p f-left">
					<div className="share-b-rating w-100p f-left">
						<div className="w-100p f-left text">Rating</div>
						<div className="w-100p f-left star-wrapper">
							{[...Array(5)].map((value, index) => (
								<a
									key={index}
									onClick={() =>
										this.handleClickPlus(index + 1)
									}
								>
									{index < this.state.rating ? (
										<Ratingfilled />
									) : (
										<Rating />
									)}
								</a>
							))}
						</div>
					</div>
					<div className="w-100p f-left share-b-input">
						<div className="input-wrapper w-100p f-left">
							<Input
								classname={'form-input'}
								rows={1}
								autoFocus={false}
								placeholder={'Tell us what you think'}
								value={this.state.reviewText}
								onChange={(e) => this.saveReviewInput(e)}
								isInputError={this.state.isReviewInputError}
								errorMessage={this.state.reviewErrorMessage}
							/>
						</div>
					</div>
					<p>{this.state.errorMessage}</p>
				</div>
				<div className="m-footer w-100p f-left">
					<div className="share-b-submit w-100p f-left">
						<Button
							type={'primary'}
							name={'Submit your feedback'}
							onClick={() => this.validateReviewForm()}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default bottomModal;
