import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import Plusicon from '../../../../src/assets/svg/plus.svg';
import Cancel from '../../../assets/svg/close.svg';
import Submit from '../../../assets/svg/check.svg';
class addToCollections extends Component {
	constructor() {
		super();
		this.state = {
			showInput: false,
			showText: true,
			showButtons: false,
		};
	}
	showInput = () => {
		this.setState({
			showInput: true,
			showText: false,
			showButtons: true,
		});
	};
	cancelForm = () => {
		this.setState({
			showInput: false,
			showText: true,
			showButtons: false,
		});
	};
	render() {
		return (
			<div className="add-to-collection-wrapper f-left w-100p f-left">
				{/* Item 1*/}
				<div className="atc-item d-flex align-center">
					<div className="atc-item-right f-left">
						<div className="r-top w-100p f-left">
							For Print Album
						</div>
						<div className="r-bottom w-100p f-left">123 photos</div>
					</div>
				</div>
				{/* Item 2 */}
				<div className="atc-item d-flex align-center">
					<div className="atc-item-left f-left">
						<input
							type="checkbox"
							name="collection"
							value="1"
							className="form-check-input"
						/>
					</div>
					<div className="atc-item-right f-left">
						<div className="r-top w-100p f-left">
							For Print Album
						</div>
						<div className="r-bottom w-100p f-left">123 photos</div>
					</div>
				</div>

				{/* Create COllection button */}
				<div className="atc-item atc-item-create d-flex align-center">
					<div className="atc-item-left f-left d-flex align-center">
						<Plusicon />
					</div>
					<div className="atc-item-right f-left">
						<div
							className="atc-text"
							style={{
								display: this.state.showText ? 'block' : 'none',
							}}
							onClick={() => this.showInput()}
						>
							Create New Collection
						</div>
						<div
							className="atc-input"
							style={{
								display: this.state.showInput
									? 'block'
									: 'none',
							}}
						>
							<input
								type="text"
								placeholder={'Name your collection'}
							/>
						</div>
					</div>
				</div>

				{/* Buttons Holder */}
				<div
					className="submit-button-wrapper w-100p f-left"
					style={{
						display: this.state.showButtons ? 'block' : 'none',
					}}
				>
					<div className="submit-button-container">
						<div className="s-button f-left d-flex align-center">
							<Cancel onClick={() => this.cancelForm()} />
						</div>
						<div className="s-button submit-b f-left d-flex align-center">
							<Submit />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default addToCollections;
