import React, { Component } from 'react';
import Favicon from '../../../assets/svg/favoutlined.svg';
import Moreoptions from '../../../assets/svg/moreoptions.svg';
import Edit from '../../../assets/svg/edit.svg';
import Delete from '../../../assets/svg/delete.svg';

import { withRouter } from 'next/router';
import Cancel from '../../../assets/svg/close.svg';
import Submit from '../../../assets/svg/check.svg';
import ProjectController from '../../../controllers/project';
import Modal from '../uicomponents/modals/index';
import CenterModal from '../uicomponents/modals/deleteModal';
var validator = require('validator');

class CollectionItem extends ProjectController {
	constructor() {
		super();
		this.state = {
			showdropdown: false,
			isDisplayInput: false,
			collectionName: '',
			isShowDeleteModal: false,
		};
		this.dropdownref = React.createRef();
		this.moreoptionsref = React.createRef();

		this.createCollectionInput = React.createRef();
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
	};

	componentDidUpdate = () => {
		if (this.state.isDisplayInput === true) {
			this.createCollectionInput.current.focus();
		}
	};

	dropDownOptions = (event) => {
		if (event === 'rename') {
			this.setState({
				isDisplayInput: true,
				collectionName:
					this.state.collectionName === ''
						? this.props.collection.title
						: this.state.collectionName,
				showdropdown: false,
			});
		} else if (event === 'delete') {
			this.setState({
				isShowDeleteModal: !this.state.isShowDeleteModal,
			});
		}
	};

	navigateToFavouritesScreen = () => {
		this.props.router.push(
			`/${this.props.projectSlug}/favourites/${this.props.collection._id}`
		);
	};

	toggleDropDown = () => {
		this.setState({
			showdropdown: !this.state.showdropdown,
		});
	};

	cancelForm = () => {
		this.setState({
			isDisplayInput: false,
			collectionName: this.props.collection.title,
		});
	};

	closeDeleteModal = () => {
		this.setState({
			isShowDeleteModal: false,
			showdropdown: false,
		});
		this.props.updateCollections();
	};

	validateCreateCollection = () => {
		var pattern = /^[a-zA-Z0-9 ]+$/;
		this.setState({
			isCreateCollectionLoading: true,
		});
		if (validator.isEmpty(this.state.collectionName)) {
			this.setState({
				errorcollectionName: true,
				errorcollectionNameMessage: 'Required Field',
				isCreateCollectionLoading: false,
			});
		} else if (!pattern.test(this.state.collectionName)) {
			this.setState({
				errorcollectionName: true,
				errorcollectionNameMessage: 'Enter Valid Collection Name',
				isCreateCollectionLoading: false,
			});
		} else {
			this.setState({
				errorcollectionName: false,
				errorcollectionNameMessage: '',
				isCreateCollectionLoading: false,
			});
		}

		if (
			!validator.isEmpty(this.state.collectionName) &&
			pattern.test(this.state.collectionName)
		) {
			this.updateCollectionName(this.props.collection._id);
		}
	};

	render() {
		return (
			<React.Fragment>
				<div
					className="collection-item w-100p f-left d-flex align-center"
					key={this.props.key}
				>
					<div
						className="cover-image f-left d-flex align-center"
						onClick={() => this.navigateToFavouritesScreen()}
					>
						{/* <img src={imageURL} /> */}
						<Favicon />
					</div>

					{this.state.isDisplayInput === false ? (
						<div
							className="text-wrapper f-left "
							onClick={() => this.navigateToFavouritesScreen()}
						>
							<div className="t-top w-100p f-left">
								{this.state.collectionName === ''
									? this.props.collection.title
									: this.state.collectionName}
							</div>
							<div className="t-bottom w-100p f-left">
								{_.size(this.props.collection.image_ids)} Photos
							</div>
						</div>
					) : (
						<div className="atc-input">
							<input
								type="text"
								placeholder={'Name your collection'}
								value={this.state.collectionName}
								ref={this.createCollectionInput}
								onChange={(e) =>
									this.setState({
										collectionName: e.target.value,
									})
								}
							/>
						</div>
					)}

					<div
						className="more-options"
						ref={this.moreoptionsref}
						onClick={(e) => this.toggleDropDown(e)}
					>
						<Moreoptions />
					</div>

					<div
						className="collection-dropdown"
						style={{
							display: this.state.showdropdown ? 'block' : 'none',
						}}
					>
						<a
							className="w-100p f-left d-flex align-center"
							onClick={() => this.dropDownOptions('rename')}
						>
							<Edit />
							Rename
						</a>
						<a
							className="w-100p f-left d-flex align-center"
							onClick={() => this.dropDownOptions('delete')}
						>
							<Delete className="delete" />
							Delete
						</a>
					</div>
				</div>
				<div
					className="submit-button-wrapper w-100p f-left"
					style={{
						display: this.state.isDisplayInput ? 'block' : 'none',
					}}
				>
					{this.state.isCreateCollectionLoading ? (
						<Loader type="image" />
					) : (
						<div className="submit-button-container">
							<div className="s-button f-left d-flex align-center">
								<Cancel onClick={() => this.cancelForm()} />
							</div>
							<div className="s-button submit-b f-left d-flex align-center">
								<Submit
									onClick={() =>
										this.validateCreateCollection()
									}
								/>
							</div>
						</div>
					)}
				</div>
				{this.state.isDisplayInput ? (
					<p className="error-message">
						{this.state.errorcollectionNameMessage}
					</p>
				) : (
					''
				)}
				<Modal
					show={this.state.isShowDeleteModal}
					modalType={'center'}
					handleClose={() => this.dropDownOptions('delete')}
				>
					<CenterModal
						collectionID={this.props.collection._id}
						handleClose={() => this.closeDeleteModal()}
					/>
				</Modal>
			</React.Fragment>
		);
	}
}

export default withRouter(CollectionItem);
