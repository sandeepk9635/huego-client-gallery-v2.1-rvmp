import React, { Component } from 'react';
import Delete from '../../../assets/svg/delete.svg';
const moment = require('moment');

class suggesteditItem extends Component {
	render() {
		let id = this.props.data._id;
		return (
			<div className="suggest-edit-item w-100p f-left">
				<div className="item-top w-100p f-left">
					<div className="left-d f-left w-50p d-flex align-center">
						{this.props.type !== 'version' ? (
							<div className="count d-flex align-center">
								<span>v{this.props.versionKey}</span>
							</div>
						) : (
							''
						)}

						<div className="last-updated">
							{moment(
								parseInt(id.toString().substr(0, 8), 16) * 1000
							).fromNow()}
						</div>
					</div>
					<div className="right-d  f-left w-100p">
						<a
							className="delete-icon"
							onClick={() =>
								this.props.deleteSuggestEditForImage(id)
							}
						>
							<Delete />
						</a>
					</div>
				</div>
				<div className="item-bottom w-100p f-left">
					{this.props.data.comment}
				</div>
			</div>
		);
	}
}

export default suggesteditItem;
