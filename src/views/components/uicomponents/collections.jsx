import React, { Component } from 'react';

import Plusicon from '../../../assets/svg/plus.svg';

import Cancel from '../../../assets/svg/close.svg';
import Submit from '../../../assets/svg/check.svg';
import ProjectController from '../../../controllers/project';
import Loader from '../global/loader';
import _ from 'lodash';
import CollectionItem from './collectionItem';
var validator = require('validator');

class Collections extends ProjectController {
	constructor() {
		super();
		this.state = {
			showInput: false,
			showText: true,
			showButtons: false,
			isCollectionLoading: true,
			collections: {},
			collectionName: '',
			isAddCollectionLoading: [],
		};

		this.createCollectionInput = React.createRef();
	}

	componentDidMount = () => {
		this.getCollections();
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
	};

	componentDidUpdate = () => {
		if (this.state.showInput === true) {
			this.createCollectionInput.current.focus();
		}
	};

	showInput = () => {
		this.setState({
			showInput: true,
			showText: false,
			showButtons: true,
		});
	};

	cancelForm = () => {
		this.setState({
			showInput: false,
			showText: true,
			showButtons: false,
			createCollection: '',
			errorcollectionName: false,
			errorcollectionNameMessage: '',
		});

		this.createCollectionInput.current.autofocus = false;
	};

	validateCreateCollection = () => {
		var pattern = /^[a-zA-Z0-9 ]+$/;
		this.setState({
			isCreateCollectionLoading: true,
		});
		if (validator.isEmpty(this.state.collectionName)) {
			this.setState({
				errorcollectionName: true,
				errorcollectionNameMessage: 'Required Field',
				isCreateCollectionLoading: false,
			});
		} else if (!pattern.test(this.state.collectionName)) {
			this.setState({
				errorcollectionName: true,
				errorcollectionNameMessage: 'Enter Valid Collection Name',
				isCreateCollectionLoading: false,
			});
		} else {
			this.setState({
				errorcollectionName: false,
				errorcollectionNameMessage: '',
				isCreateCollectionLoading: false,
			});
		}

		if (
			!validator.isEmpty(this.state.collectionName) &&
			pattern.test(this.state.collectionName)
		) {
			this.createCollection();
		}
	};

	renderCollections = () => {
		let collectionMap = _.map(this.state.collections, (collection, key) => {
			return (
				<React.Fragment>
					{this.props.collectionType === 'view' ? (
						<CollectionItem
							collection={collection}
							key={key}
							projectSlug={this.props.projectSlug}
							updateCollections={() => this.getCollections()}
						/>
					) : (
						<div
							className="collection-item w-100p f-left d-flex align-center"
							key={key}
						>
							<div className="atc-item-left f-left">
								{_.indexOf(
									this.state.isAddCollectionLoading,
									collection._id
								) >= 0 ? (
									<Loader type={'image'} />
								) : (
									<input
										type="checkbox"
										name="collection"
										className="form-check-input"
										checked={
											_.indexOf(
												collection.image_ids,
												this.props.imageID
											) >= 0
												? true
												: false
										}
										onClick={() =>
											_.indexOf(
												collection.image_ids,
												this.props.imageID
											) == -1
												? this.addImageToCollections(
														this.props.imageID,
														collection._id
												  )
												: this.removeImagefromCollections(
														this.props.imageID,
														collection._id
												  )
										}
									/>
								)}
							</div>

							<div className="text-wrapper f-left ">
								<div className="t-top w-100p f-left">
									{collection.title}
								</div>
								<div className="t-bottom w-100p f-left">
									{_.size(collection.image_ids)} Photos
								</div>
							</div>
						</div>
					)}
				</React.Fragment>
			);
		});

		return collectionMap;
	};

	render() {
		return (
			<div className="collections-wrapper w-100p f-left">
				{this.state.isCollectionLoading ? (
					<Loader type="image" />
				) : (
					this.renderCollections()
				)}

				{/* Create COllection button */}
				<div className="collection-item w-100p f-left d-flex align-center">
					{this.props.collectionType === 'view' ? (
						<div className="cover-image f-left d-flex align-center">
							<Plusicon />
						</div>
					) : (
						<div className="atc-item-left f-left d-flex align-center">
							<Plusicon />
						</div>
					)}
					<div
						className="text-wrapper f-left "
						style={{
							display: this.state.showText ? 'block' : 'none',
						}}
						onClick={() => this.showInput()}
					>
						<div className="t-top w-100p f-left">
							Create New Collection
						</div>
					</div>
					<div
						className="atc-input"
						style={{
							display: this.state.showInput ? 'block' : 'none',
						}}
					>
						<input
							type="text"
							placeholder={'Name your collection'}
							value={this.state.collectionName}
							ref={this.createCollectionInput}
							onChange={(e) =>
								this.setState({
									collectionName: e.target.value,
								})
							}
						/>
					</div>
					<div className="more-options"></div>
				</div>

				{/*
				TODO : Error Message Display in Collections
				
			*/}
				<p className="error-message">
					{this.state.errorcollectionNameMessage}
				</p>
				{/* Buttons Holder */}
				<div
					className="submit-button-wrapper w-100p f-left"
					style={{
						display: this.state.showButtons ? 'block' : 'none',
					}}
				>
					{this.state.isCreateCollectionLoading ? (
						<Loader type="image" />
					) : (
						<div className="submit-button-container">
							<div className="s-button f-left d-flex align-center">
								<Cancel onClick={() => this.cancelForm()} />
							</div>
							<div className="s-button submit-b f-left d-flex align-center">
								<Submit
									onClick={() =>
										this.validateCreateCollection()
									}
								/>
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default Collections;
