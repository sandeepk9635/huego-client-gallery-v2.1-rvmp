import React, { Component } from 'react';

import ProjectController from '../../../controllers/project';
import LazyLoad from 'react-lazyload';
import { withRouter } from 'next/router';
import Masonry from 'masonry-layout';
import Loader from '../global/loader';

import Visible from '../../../assets/svg/visible.svg';
import Invisible from '../../../assets/svg/invisible.svg';
import Favred from '../../../assets/svg/favred.svg';

import dynamic from 'next/dynamic';

class masonryGallery extends ProjectController {
	constructor() {
		super();
		this.state = {
			windowWidth: 0,
			noOfImagesInRow: 2,
			moveToLastImage: false,
		};
	}
	componentDidMount = () => {
		this.setState({
			windowWidth: window.innerWidth,
		});
		if (window.innerWidth >= 768) {
			this.setState({
				noOfImagesInRow: 4,
			});
		}
	};

	componentDidUpdate = () => {
		this.masonryView();
	};

	masonryView = () => {
		const grid = document.querySelector('.grid');

		const msnry = new Masonry(grid, {
			itemSelector: '.grid-item',

			transitionDuration: '0.8s',
			gutter: 3,
			originLeft: true,
		});

		msnry.layout();
		this.scrollToLastViewedImage();
	};

	scrollToLastViewedImage = () => {
		let lastViewedImages = localStorage.getItem('lastViewedImage')
			? JSON.parse(localStorage.getItem('lastViewedImage'))
			: [];

		let imageIndex = _.findIndex(lastViewedImages, {
			projectID: this.props.router.query.projectSlug,
			albumID: this.props.router.query.albumID,
			galleryID: this.props.router.query.galleryID,
		});

		if (imageIndex > -1 && this.state.moveToLastImage === false) {
			let imageName =
				this.props.router.query.galleryID +
				'_' +
				lastViewedImages[imageIndex]['imageID'];

			this[`${imageName}`].current.scrollIntoView();

			this.setState({
				moveToLastImage: true,
			});
		}
	};

	renderGaleryImageOptions = (imageID) => {
		if (
			_.has(this.props.imagesStatus, imageID) == false ||
			_.has(this.props.imagesStatus[imageID], 'collections') === false ||
			_.has(this.props.imagesStatus[imageID], 'isPrivate') === false
		)
			return '';
		else
			return this.props.imagesStatus[imageID]['collections'] === 0 &&
				this.props.imagesStatus[imageID]['isPrivate'] === false ? (
				''
			) : (
				<div
					className={`image-features ${
						this.props.imagesStatus[imageID]['collections'] === 0 ||
						this.props.imagesStatus[imageID]['isPrivate'] === false
							? 'image-features-w-50'
							: 'image-features-w-100'
					}`}
				>
					{this.props.imagesStatus[imageID]['collections'] === 0 ? (
						''
					) : (
						<div
							className={`fav ${
								this.props.imagesStatus[imageID][
									'isPrivate'
								] === false
									? 'w-100p'
									: 'w-50p'
							} f-left d-flex align-center`}
						>
							<Favred />
							{this.props.imagesStatus[imageID]['collections']}
						</div>
					)}

					<div
						className={`visibility ${
							this.props.imagesStatus[imageID]['collections'] ===
							0
								? 'w-100p'
								: 'w-50p'
						} f-left d-flex align-center`}
					>
						{this.props.imagesStatus[imageID]['isPrivate'] ? (
							<Invisible />
						) : (
							''
						)}
					</div>
				</div>
			);
	};

	renderFavouritesImageOptions = (imageID) => {
		if (
			_.has(this.props.imagesStatus, imageID) == false ||
			_.has(this.props.imagesStatus[imageID], 'collections') === false
		)
			return '';
		else
			return this.props.imagesStatus[imageID]['collections'] <= 1 ? (
				''
			) : (
				<div className={`image-features image-features-w-50`}>
					<div className={`fav w-100p f-left d-flex align-center`}>
						<Favred />
						{this.props.imagesStatus[imageID]['collections']}
					</div>
				</div>
			);
	};

	renderSingleImage = (image, imageID) => {
		let url =
			this.props.signedUrl['baseUrl'] +
			'thumbnails-300w/' +
			image.givenFileName +
			'?Key-Pair-Id=' +
			this.props.signedUrl.credentials['Key-Pair-Id'] +
			'&Policy=' +
			this.props.signedUrl.credentials['Policy'] +
			'&Signature=' +
			this.props.signedUrl.credentials['Signature'];

		return (
			<div className="gridImageContainer">
				<div className="image-wrapper">
					{this.props.view === 'gallery'
						? this.renderGaleryImageOptions(imageID)
						: this.props.view === 'favourites'
						? this.renderFavouritesImageOptions(imageID)
						: ''}
					<img src={url} alt={image.givenFileName} />
				</div>
			</div>
		);
	};

	renderGalleryImages = () => {
		let count = 0;

		let displayImagesArray = {};
		if (this.props.view === 'gallery') {
			displayImagesArray = this.props.images;
		} else if (this.props.view === 'favourites') {
			displayImagesArray = this.props.images;
		}

		let galleryView = _.map(displayImagesArray, (images, key) => {
			let value = this.props.router.query.galleryID + '_' + images['_id'];
			this[`${value}`] = React.createRef();

			count++;
			let image = {};
			if (this.props.view === 'gallery') {
				image = _.filter(images.versions, {
					versionId: images.activeVersionId,
					isUploaded: true,
					isProcessed: true,
				});

				if (_.size(image) === 0) return null;
				image = image[0];
			} else if (this.props.view === 'favourites') {
				image = images;
			}

			let thumbWidth =
				(this.state.windowWidth - this.state.noOfImagesInRow * 3) /
				this.state.noOfImagesInRow;
			let thumHeight =
				(thumbWidth / image.originalWidth) * image.originalHeight;

			let imageID =
				this.props.view === 'gallery' ? images._id : images.image_id;

			return (
				<div
					className={`grid-item `}
					key={key}
					style={{ height: thumHeight, width: thumbWidth }}
					data-imageName={image.givenFileName}
					ref={this[`${this.props.selectedGallery + '_' + imageID}`]}
					onClick={() => this.props.navigateImage(imageID)}
				>
					{count < 10 ? (
						this.renderSingleImage(image, imageID)
					) : (
						<LazyLoad
							once={true}
							height={thumHeight}
							offset={[100, 300]}
							placeholder={
								<div style={{ height: thumHeight }}>
									<Loader type={'image'} />
								</div>
							}
						>
							{this.renderSingleImage(image, imageID)}
						</LazyLoad>
					)}
				</div>
			);
		});

		return <div className="grid">{galleryView}</div>;
	};

	render() {
		return (
			<div className="masonry-gallery">{this.renderGalleryImages()}</div>
		);
	}
}

export default withRouter(masonryGallery);
