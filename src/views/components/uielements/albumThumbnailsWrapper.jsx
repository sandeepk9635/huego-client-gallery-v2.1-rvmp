import React, { Component } from 'react';
import Next from '../../../assets/svg/next.svg';
import Prev from '../../../assets/svg/prev.svg';
import Slider from 'react-slick';
import Loader from '../global/loader';

import _ from 'lodash';

class AlbumThumbnailsWrapper extends Component {
	constructor(props) {
		super(props);

		this.state = {
			settings: {
				infinite: false,
				speed: 500,
				nextArrow: <Next />,
				prevArrow: <Prev />,
			},
			showArrows: false,
			isAlbumIconDownloading: false,
		};

		this.slider = React.createRef();
	}

	componentDidMount = () => {
		if (this.props.windowWidth >= 768) {
			this.setState({
				showArrows: true,
			});
		}

		if (_.size(this.props.albums) > 0) {
			this.downloadAlbumIcons();
		}
	};

	componentWillReceiveProps = (nextProps) => {
		if (
			nextProps.albums != this.props.albums &&
			this.state.isAlbumIconDownloading === false
		) {
			this.downloadAlbumIcons();
		}
		if (nextProps != this.props) {
			this.slider.slickGoTo(nextProps.isActive);
		}
	};
	downloadAlbumIcons = () => {
		this.setState({
			isAlbumIconDownloading: true,
		});
		let loadingImageDownloaded = [];
		_.map(this.props.albums, (album, key) => {
			let url =
				this.props.signedUrl['baseUrl'] +
				'thumbnails-100h/' +
				album.coverImage.givenFileName +
				'?Key-Pair-Id=' +
				this.props.signedUrl['credentials']['Key-Pair-Id'] +
				'&Signature=' +
				this.props.signedUrl['credentials']['Signature'] +
				'&Policy=' +
				this.props.signedUrl['credentials']['Policy'];

			let loadingImage = new Image();
			loadingImage.src = url;
			loadingImage.onload = () => {
				loadingImageDownloaded[album._id] = true;

				this.setState({
					loadingImageDownloaded,
				});
			};
		});
	};
	renderAlbumsIcons = () => {
		let albums = _.map(this.props.albums, (album, key) => {
			if (album.isPublished === true)
				return (
					<div
						className={'album-thumbnail'}
						key={key}
						onClick={() => this.props.onClick(key)}
					>
						{_.has(this.state.loadingImageDownloaded, album._id) ===
						false ? (
							<a className="thumb loader-thumb">
								<Loader bg={false} type={'image'} />
							</a>
						) : (
							<a
								className={`${
									this.props.isActive === key
										? 'active thumb'
										: 'thumb'
								}`}
							>
								<img
									src={
										this.props.signedUrl['baseUrl'] +
										'thumbnails-100h/' +
										album.coverImage.givenFileName +
										'?Key-Pair-Id=' +
										this.props.signedUrl['credentials'][
											'Key-Pair-Id'
										] +
										'&Signature=' +
										this.props.signedUrl['credentials'][
											'Signature'
										] +
										'&Policy=' +
										this.props.signedUrl['credentials'][
											'Policy'
										]
									}
								/>
							</a>
						)}
						<span className="w-100p f-left text-center">
							{album.title}
						</span>
					</div>
				);
		});

		return albums;
	};
	render() {
		var settings = {
			...this.state.settings,
			initialSlide: this.props.isActive,
			arrows: this.state.showArrows,
		};

		return (
			<div className={'albums-thumbnails-wrapper'}>
				<div className={'albums-thumbnails-container'}>
					{/* {this.props.windowWidth >= 768 ? (
						<span className="prev">
							<Prev />
						</span>
					) : (
						''
					)} */}

					<div className={'albums-thumbnails-subwrapper'}>
						<Slider
							{...settings}
							slidesToShow={parseInt(
								(this.props.windowWidth >= 768
									? 320
									: this.props.windowWidth - 20) / 75
							)}
							slidesToScroll={parseInt(
								(this.props.windowWidth >= 768
									? 320
									: this.props.windowWidth - 20) / 75
							)}
							ref={(slider) => (this.slider = slider)}
						>
							{this.renderAlbumsIcons()}
						</Slider>
					</div>

					{/* {this.props.windowWidth >= 768 ? (
						<span className="next">
							<Next />
						</span>
					) : (
						''
					)} */}
				</div>
			</div>
		);
	}
}

export default AlbumThumbnailsWrapper;
