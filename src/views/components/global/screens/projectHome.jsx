import React, { Component } from 'react';
import ShareIcon from '../../../../assets/svg/shareicon.svg';
import DownloadIcon from '../../../../assets/svg/downloadicon.svg';
import LockIcon from '../../../../assets/svg/privacyicon.svg';
import UnlockIcon from '../../../../assets/svg/unlock.svg';
import Visible from '../../../../assets/svg/visible.svg';
import Invisible from '../../../../assets/svg/invisible.svg';
import Button from '../button';

import ProjectController from '../../../../controllers/project';
import Loader from '../../../components/global/loader';
import jwt_decode from 'jwt-decode';

class projectHome extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			isLoadingPrivacy: false,
			isGuestUser: false,
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
		this.checkUserIsGuest();

		// let userToken = localStorage.getItem('usertoken');
		// var decoded = jwt_decode(userToken);
		// if (decoded.userRole === 'guest') {
		// 	this.setState({
		// 		isGuestUser: true,
		// 	});
		// }
	};

	componentWillReceiveProps = (nextProps) => {
		if (this.props.isLoadingPrivacy !== this.state.isLoadingPrivacy) {
			this.setState({
				isLoadingPrivacy: nextProps.isLoadingPrivacy,
			});
		}
	};

	render() {
		return (
			<div className="project-home-info-wrapper w-100p f-left">
				{this.props.albumsCount === 'loading' ? (
					<div className="item-info w-100p f-left">
						<div className="w-50p f-left project-album-hp-loader">
							{/* Replace loader with text inside span and replace className text-right */}
							<span className="f-right">
								<Loader type={'image'} bg={false} />
							</span>
						</div>
						<div className="w-50p f-left text-left">
							<span>
								{this.props.screenName == 'albumHome'
									? ' Photos'
									: ' Albums'}
							</span>
						</div>
					</div>
				) : (
					<div className="item-info w-100p f-left">
						<div className="w-100p f-left align-center">
							<span>
								{this.props.albumsCount}{' '}
								{this.props.screenName == 'albumHome'
									? 'Photos'
									: 'Albums'}
							</span>
						</div>
					</div>
				)}

				<div className={'icons-container-wrapper w-100p f-left'}>
					<div className={'icons-container'}>
						{this.state.isGuestUser ? (
							''
						) : this.props.screenName == 'projectHome' ||
						  this.props.screenName == 'albumHome' ? (
							<a
								className={'icon'}
								onClick={() => this.props.onClickRouteToShare()}
							>
								<ShareIcon />
							</a>
						) : (
							''
						)}

						{this.props.screenName == 'albumHome' ||
						this.props.screenName == 'gallery' ? (
							<React.Fragment>
								{this.state.isGuestUser ? (
									''
								) : this.state.isLoadingPrivacy ? (
									<a className="icon">
										<Loader type={'image'} bg={false} />
									</a>
								) : (
									<a
										className={'icon lock-unlock-icon'}
										onClick={() =>
											this.updateAlbumPrivacy(
												!this.props.albumDetails
													.guestAccess.isEnabled
											)
										}
									>
										{_.has(
											this.props.albumDetails,
											'guestAccess'
										) &&
										this.props.albumDetails.guestAccess
											.isEnabled ? (
											<UnlockIcon className="lock" />
										) : (
											<LockIcon className="unlock" />
										)}
									</a>
								)}
								{this.props.isDownloadLoading ? (
									<a className="icon">
										<Loader type={'image'} bg={false} />
									</a>
								) : this.props.allowDownloads ? (
									<a
										className={'icon'}
										onClick={() =>
											this.props.downloadAlbumImages()
										}
									>
										<DownloadIcon />
									</a>
								) : (
									''
								)}
							</React.Fragment>
						) : (
							''
						)}
					</div>
				</div>
				{(this.props.screenName == 'albumHome' &&
					this.props.albumsCount > 0) ||
				this.props.screenName == 'gallery' ? (
					<div className="view-photos-button">
						<Button
							type={'primary'}
							name={'View Photos'}
							onClick={() => this.props.onClickViewPhotos()}
						/>
					</div>
				) : (
					''
				)}
			</div>
		);
	}
}

export default projectHome;
