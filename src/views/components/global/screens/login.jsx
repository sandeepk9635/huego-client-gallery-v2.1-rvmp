import React, { Component } from 'react';
import Button from '../button';

import { withRouter } from 'next/router';
import Link from 'next/link';

import LoginController from '../../../../controllers/login';
class login extends LoginController {
	constructor(props) {
		super(props);
		this.state = {
			loginType: 'master',
		};
	}

	componentDidMount = () => {
		if (_.has(this.props.router.query, 'loginType')) {
			this.setState({
				loginType: this.props.router.query.loginType,
			});
		}
	};

	routerToPin = (loginType) => {
		if (
			loginType === 'guest' &&
			this.props.projectData.guestAccess.pinRequired === false
		) {
			this.verifyProjectGuestPin(
				_.has(this.props.query, 'from') ? this.props.query.from : ''
			);
		} else {
			let url = loginType === 'master' ? 'master-pin' : 'guest-pin';

			let redirectURL = `/${this.props.router.query.projectSlug}/login/${url}`;

			if (_.has(this.props.query, 'from')) {
				redirectURL = redirectURL + '?from=' + this.props.query.from;
			}

			this.props.router.push(redirectURL);
		}
	};

	routerAlbumPin = () => {
		if (this.props.albumData.guestAccess.pinRequired === false) {
			this.verifyAlbumLoginPin(
				_.has(this.props.query, 'from') ? this.props.query.from : ''
			);
		} else {
			let redirectURL = `/${this.props.router.query.projectSlug}/${this.props.router.query.albumID}/login/pin`;

			if (_.has(this.props.query, 'from')) {
				redirectURL = redirectURL + '?from=' + this.props.query.from;
			}

			this.props.router.push(redirectURL);
		}
	};

	render() {
		return (
			<div className={'buttons-wrapper'}>
				<Button
					type={
						this.state.loginType === 'master'
							? 'primary'
							: 'secondary'
					}
					name={
						this.props.screenName === 'login'
							? 'Master Login'
							: 'Login'
					}
					onClick={() =>
						this.props.screenName === 'login'
							? this.routerToPin('master')
							: this.routerAlbumPin()
					}
				/>

				{this.props.projectData.guestAccess.enabled === true &&
				this.props.screenName === 'login' ? (
					<Button
						type={
							this.state.loginType === 'guest'
								? 'primary'
								: 'secondary'
						}
						name={'Guest Login'}
						onClick={() => this.routerToPin('guest')}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

export default withRouter(login);
