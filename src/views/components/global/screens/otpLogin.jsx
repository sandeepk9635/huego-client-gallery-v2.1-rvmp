import React, { Component } from 'react';
import OtpInput from 'react-otp-input';

import { withRouter } from 'next/router';
import Link from 'next/link';

import LoginController from '../../../../controllers/login';

class otpLogin extends LoginController {
	constructor(props) {
		super(props);
		this.state = {
			otp: '',
			loginType: props.router.query.loginType,
			isOTPDisabled: false,
		};
	}

	componentDidMount = () => {
		let tenantID = window.location.hostname.split('.huego.com');
		tenantID = _.size(tenantID) > 1 ? tenantID[0] : 'bhogesh';
		this.setState({
			tenantID,
		});
	};

	handleChange = (otp) => {
		this.setState({ otp, errorMessage: '' }, () => {
			if (otp.length === 6 && this.state.loginType === 'master-pin') {
				this.setState({
					isOTPDisabled: true,
					isLoading: true,
				});
				this.verifyProjectMasterPin(
					_.has(this.props.query, 'from') ? this.props.query.from : ''
				);
			} else if (
				otp.length === 4 &&
				this.state.loginType === 'guest-pin'
			) {
				this.setState({
					isOTPDisabled: true,
					isLoading: true,
				});
				this.verifyProjectGuestPin(
					_.has(this.props.query, 'from') ? this.props.query.from : ''
				);
			} else if (otp.length === 4 && this.state.loginType === 'pin') {
				this.setState({
					isOTPDisabled: true,
					isLoading: true,
				});
				this.verifyAlbumLoginPin(
					_.has(this.props.query, 'from') ? this.props.query.from : ''
				);
			}
		});
	};

	toggleUsers = () => {
		if (
			this.state.loginType === 'master-pin' &&
			this.props.projectData.guestAccess.pinRequired === false
		) {
			this.verifyProjectGuestPin();
		} else {
			let url =
				this.state.loginType === 'master-pin'
					? `/${this.props.router.query.projectSlug}/login/guest-pin`
					: `/${this.props.router.query.projectSlug}/login/master-pin`;

			this.setState({
				otp: '',
				loginType:
					this.props.router.query.loginType === 'master-pin'
						? 'guest-pin'
						: 'master-pin',
			});
			this.props.router.replace(url);
		}
	};
	render() {
		return (
			<div className="otp-wrapper">
				<div className="otp-help-text text-center w-100p f-left">
					Enter your{' '}
					{this.state.loginType === 'master-pin' ? 'master' : 'guest'}{' '}
					PIN
				</div>
				<OtpInput
					value={this.state.otp}
					onChange={this.handleChange}
					numInputs={
						this.state.loginType === 'master-pin' ? '6' : '4'
					}
					containerStyle={
						this.state.loginType === 'master-pin'
							? 'otp-container'
							: 'otp-container otp-guest'
					}
					focusStyle={'focus-style'}
					isDisabled={this.state.isOTPDisabled}
					isInputNum={true}
				/>
				<p
					className="error-message"
					style={{ marginTop: '10px', height: '10px' }}
				>
					{this.state.errorMessage}
				</p>
				{(this.state.loginType === 'master-pin' &&
					this.props.projectData.guestAccess.enabled === true) ||
				this.state.loginType === 'guest-pin' ? (
					<div className="otp-login-guest">
						Not a{' '}
						{this.state.loginType === 'master-pin'
							? 'Master'
							: 'Guest'}{' '}
						User?{' '}
						<span onClick={() => this.toggleUsers()}>
							Login as{' '}
							{this.state.loginType === 'master-pin'
								? 'guest'
								: 'master'}{' '}
						</span>
					</div>
				) : (
					''
				)}
			</div>
		);
	}
}

export default withRouter(otpLogin);
