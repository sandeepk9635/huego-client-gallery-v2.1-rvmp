import React, { Component } from 'react';
import _ from 'lodash';
{
	/*

	Props Definition
	----------------------------
	type = input type
	value = input value
	name = input name
	label =  display text
	isInputError = [true/false], displays the error warning
	errorMessage = text, error Message during validation

*/
}

//TODO : No Error style after passing props

class Input extends Component {
	render() {
		return (
			<div
				className={
					this.props.isInputError
						? 'error-input ' + this.props.classname + ''
						: this.props.isInputNoError
						? 'no-error-input ' + this.props.classname
						: this.props.classname
				}
			>
				<textarea
					type={this.props.type}
					className="inputfield"
					name={this.props.name}
					required
					value={this.props.value}
					autoFocus={
						_.has(this.props, 'autoFocus')
							? this.props.autoFocus
							: false
					}
					placeholder={this.props.placeholder}
					rows={this.props.type === 'textarea' ? this.props.rows : 0}
					onChange={(e) => this.props.onChange(e)}
				/>

				{this.props.isInputError ? (
					<span className="error-message">
						{this.props.errorMessage}
					</span>
				) : (
					<span></span>
				)}
				{this.props.isInputNoError ? (
					<span className="no-error-message">
						{this.props.noerrorMessage}
					</span>
				) : (
					<span></span>
				)}
			</div>
		);
	}
}

export default Input;
