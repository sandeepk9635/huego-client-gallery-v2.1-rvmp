import React, { Component } from 'react';
// import '../../../assets/scss/home.scss';
import Loader from './loader';
class bgWrapper extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShowBG: false,
		};
	}

	componentDidMount = () => {
		let loadingImage = new Image();
		loadingImage.src = this.props.bgImage;
		loadingImage.onload = () => {
			this.setState({
				isShowBG: true,
			});
		};
	};

	render() {
		return (
			<div className="bg-wrapper w-100p h-100p f-left">
				<div className="bg-image w-100p h-100p">
					<a className="w-100p h-100p f-left">
						{this.state.isShowBG === false ? (
							<Loader type={'image'} bg={false} />
						) : (
							<img src={this.props.bgImage} />
						)}
					</a>
				</div>
				<div className="gradient-overlay w-100p h-100p"></div>
			</div>
		);
	}
}

export default bgWrapper;
