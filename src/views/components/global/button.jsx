import React, { Component } from 'react';

class button extends Component {
	render() {
		return (
			<a
				className={'button text-center f-left ' + this.props.type}
				href={this.props.href}
				onClick={this.props.onClick}
				style={{
					background: this.props.danger === true ? 'red' : '',
					color: this.props.danger === true ? 'white' : '',
				}}
			>
				{this.props.name}
			</a>
		);
	}
}

export default button;
