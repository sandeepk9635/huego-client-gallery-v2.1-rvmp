import _ from 'lodash';
import React, { Component } from 'react';
// import homeStyles from '../../../assets/scss/home.module.scss';
import Login from './screens/login';
import OtpLogin from './screens/otpLogin';
import ProjectHome from './screens/projectHome';
class infoWrapper extends Component {
	constructor() {
		super();
		this.state = {
			windowWidth: 0,
		};
	}
	componentDidMount() {
		this.setState({
			windowWidth: window.innerWidth,
		});
	}
	render() {
		return (
			<div className="info-wrapper w-100p h-100p">
				<div
					className={
						this.state.windowWidth >= 768 &&
						(this.props.screenName === 'albumHome' ||
							this.props.screenName === 'projectHome')
							? 'info-dynamic-wrapper info-desk'
							: 'info-dynamic-wrapper'
					}
					style={{
						height:
							this.props.screenName === 'projectHome' ||
							this.props.screenName === 'albumHome' ||
							this.props.screenName === 'gallery'
								? 'fit-content !important'
								: '75%',

						bottom:
							this.props.screenName === 'projectHome' ||
							this.props.screenName === 'albumHome' ||
							this.props.screenName === 'gallery'
								? '175px'
								: '0',
					}}
				>
					<h1 className={'title text-center'}>{this.props.title}</h1>
					{_.has(this.props, 'subTitle') ? (
						<h2 className={'title text-center'}>
							{this.props.subTitle}
						</h2>
					) : (
						''
					)}

					{this.props.screenName === 'login' ||
					this.props.screenName === 'album-login' ? (
						<Login {...this.props} />
					) : this.props.screenName === 'loginOtpScreen' ? (
						<OtpLogin {...this.props} />
					) : this.props.screenName === 'projectHome' ||
					  this.props.screenName === 'albumHome' ||
					  this.props.screenName === 'gallery' ? (
						<ProjectHome {...this.props} />
					) : (
						''
					)}
				</div>
			</div>
		);
	}
}

export default infoWrapper;
