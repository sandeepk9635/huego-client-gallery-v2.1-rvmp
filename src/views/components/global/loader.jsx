import React, { Component } from 'react';
import Loader from '../../../../src/assets/svg/loader.svg';
class loader extends Component {
	render() {
		return (
			<div
				className={`page-loading-container d-flex align-center 
					
				`}
				style={{
					height: this.props.type === 'image' ? '100%' : '100vh',
					background: this.props.bg === false ? 'none' : '',
				}}
			>
				<a>
					<Loader />
				</a>
			</div>
		);
	}
}

export default loader;
