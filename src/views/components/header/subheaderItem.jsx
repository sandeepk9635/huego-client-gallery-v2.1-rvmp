import React, { Component } from 'react';
import _ from 'lodash';
class subheaderItem extends Component {
	render() {
		return (
			<a
				className={
					+_.has(this.props, 'active') && this.props.active === true
						? 'sub-header-item active'
						: 'sub-header-item'
				}
				onClick={() => this.props.onClick()}
			>
				<span>{this.props.name}</span>
			</a>
		);
	}
}

export default subheaderItem;
