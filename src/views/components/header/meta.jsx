import Head from 'next/head';
const Meta = (props) => (
	<Head>
		<title>{props.title}</title>
		<meta name="description" content={props.description} />
		<meta property="og:type" content="website" />
		<meta name="og:title" property="og:title" content={props.title} />
		<meta
			name="og:description"
			property="og:description"
			content={props.description}
		/>
		<meta property="og:site_name" content="Huego Galleries by huemn.ai" />
		<meta property="og:url" content={props.siteURL} />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content={props.title} />
		<meta name="twitter:description" content={props.description} />
		<meta name="twitter:site" content={props.siteURL} />
		<meta name="twitter:creator" content="huego" />
		<meta property="og:image" content={props.imageURL} />
		<meta name="twitter:image" content={props.imageURL} />
		<link rel="canonical" href={props.siteURL} />
	</Head>
);
export default Meta;
