import React, { Component, createRef } from 'react';
import SubheaderItem from './subheaderItem';
import _ from 'lodash';

class subheader extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div
				className={'sub-header '}
				style={{
					top:
						this.props.onscrollclassname === 'hidden'
							? '-82px'
							: '50px',
				}}
			>
				<div className="sub-header-wrapper">
					{_.map(this.props.tagsJson, (tag, key) => {
						return (
							<SubheaderItem
								name={tag.title}
								active={
									this.props.isActive === tag._id
										? true
										: false
								}
								onClick={() => this.props.onClick(tag._id)}
							/>
						);
					})}
				</div>
			</div>
		);
	}
}

export default subheader;
