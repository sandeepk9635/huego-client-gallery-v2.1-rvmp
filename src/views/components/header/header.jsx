import React, { Component } from 'react';
// import '../../../assets/scss/header.module.scss';
import Menuicon from '../../../assets/svg/mainmenu.svg';
import Favicon from '../../../assets/svg/fav.svg';
import FavRed from '../../../assets/svg/favred.svg';
import Editicon from '../../../assets/svg/edit.svg';
import Uparrowicon from '../../../assets/svg/uparrow.svg';
import Backicon from '../../../assets/svg/backbutton.svg';
import Closeicon from '../../../assets/svg/close.svg';
import Shareicon from '../../../assets/svg/share.svg';
import Button from '../global/button';

import ProjectController from '../../../controllers/project';

import _ from 'lodash';
const moment = require('moment');
class Header extends ProjectController {
	constructor(props) {
		super(props);
		this.state = {
			hleftwidth: 50 + '%',
			hcenterwidth: 0 + '%',
			hrightwidth: 50 + '%',
			isGuestUser: false,
			isGuestAlbumUser: false,
		};
	}

	componentDidMount = () => {
		this.checkUserIsGuest();
		// if (!localStorage.getItem('usertoken')) {
		// 	console.log(this.props);
		// 	return this.props.router.push(
		// 		`/${this.props.router.query.projectSlug}/login?from=${this.props.router.asPath}`
		// 	);
		// } else {

		// 	let userToken = localStorage.getItem('usertoken');
		// 	var decoded = jwt_decode(userToken);
		// 	if (decoded.userRole === 'guest') {
		// 		this.setState({
		// 			isGuestUser: true,
		// 			isGuestAlbumUser: _.has(decoded, 'album_id') ? true : false,
		// 		});
		// 	}
		// }

		this.setHeaderWidths();
	};

	componentWillReceiveProps = (nextProps) => {
		if (this.props.screenName !== nextProps.screenName) {
			this.setHeaderWidths();
		}
	};

	setHeaderWidths = () => {
		if (this.props.screenName === 'projectHome') {
			this.setState({
				hleftwidth: 50 + '%',
				hcenterwidth: 0 + '%',
				hrightwidth: 50 + '%',
			});
		} else if (this.props.screenName === 'albumHome') {
			this.setState({
				hleftwidth: 40 + '%',
				hcenterwidth: 0 + '%',
				hrightwidth: 60 + '%',
			});
		}
		if (
			this.props.screenName === 'gallery' ||
			this.props.screenName === 'favourites'
		) {
			this.setState({
				hleftwidth: 75 + '%',
				hcenterwidth: 0 + '%',
				hrightwidth: 25 + '%',
			});
		} else if (this.props.screenName === 'fullView') {
			this.setState({
				hleftwidth: 33 + '%',
				hcenterwidth: 33 + '%',
				hrightwidth: 34 + '%',
			});
		} else {
			if (this.props.screenName === 'projectShare') {
				this.setState({
					hleftwidth: 50 + '%',
					hcenterwidth: 0 + '%',
					hrightwidth: 50 + '%',
				});
			} else {
				this.setState({
					hleftwidth: 25 + '%',
					hcenterwidth: 0 + '%',
					hrightwidth: 75 + '%',
				});
			}
		}
	};
	render() {
		return (
			<div
				className={'header '}
				style={{
					position:
						this.props.screenName === 'gallery' ||
						this.props.screenName === 'favourites'
							? 'fixed'
							: 'absolute',
					top:
						this.props.onscrollclassname === 'hidden'
							? '-50px'
							: '0px',
					background:
						this.props.screenName === 'projectHome' ? 'none' : '',
				}}
			>
				<div
					className="header-left d-flex align-center"
					style={{ width: this.state.hleftwidth }}
				>
					{this.props.screenName === 'projectHome' ? (
						<a className="header-item main-menu-icon f-left">
							<Menuicon />
						</a>
					) : this.props.screenName === 'albumHome' ||
					  this.props.screenName === 'fullView' ||
					  this.props.screenName === 'fullView-favts' ? (
						<div className={'header-item f-left icon-with-text'}>
							<div className="icon">
								<Backicon
									onClick={() =>
										this.props.screenName === 'albumHome'
											? this.props.onClick('projectHome')
											: this.props.screenName ===
											  'fullView'
											? this.props.onClick('gallery')
											: this.props.screenName ===
													'fullView-favts' ||
											  this.props.screenName ===
													'projectShare'
											? this.props.onClick()
											: ''
									}
								/>
							</div>
						</div>
					) : this.props.screenName === 'gallery' ||
					  this.props.screenName === 'favourites' ? (
						<div className={'header-item f-left icon-with-text'}>
							<div className="icon">
								{this.state.isGuestAlbumUser == true ? (
									''
								) : this.props.screenName === 'gallery' ? (
									<Uparrowicon
										onClick={() =>
											this.props.onClick('albumHome')
										}
									/>
								) : this.props.screenName === 'favourites' ? (
									<a className="header-item main-menu-icon fav-icon">
										<FavRed />
									</a>
								) : (
									''
								)}
							</div>
							<div className="text">{this.props.title}</div>
						</div>
					) : this.props.screenName === 'projectShare' ? (
						<div className={'header-item f-left icon-with-text'}>
							<div className="icon">
								<Shareicon />
							</div>
							<div className="text">{this.props.title}</div>
						</div>
					) : (
						''
					)}
				</div>

				{this.props.screenName === 'fullView' ? (
					<div
						className="header-center d-flex align-center"
						style={{
							width: this.state.hcenterwidth,
						}}
					>
						<div className="center-text-holder">
							<div className="file-name w-100p f-left text-center">
								{this.props.title}
							</div>

							<div className="date-time w-100p f-left text-center">
								{/* 23 Feb 2021, 09:27 AM */}

								{moment
									.unix(this.props.dateTimeEpoch)
									.format('DD MMM YYYY, hh:mm A')}
							</div>
						</div>
					</div>
				) : (
					''
				)}
				{this.state.isGuestUser ? (
					''
				) : (
					<div
						className="header-right d-flex align-center"
						style={{ width: this.state.hrightwidth }}
					>
						{this.props.screenName === 'albumHome' &&
						this.props.showFeedBack === true ? (
							<div
								className={'tertiary-button'}
								onClick={() => this.props.toggleFeedback()}
							>
								<Button
									type={'tertiary'}
									name={'Share your feedback'}
								/>
							</div>
						) : (
							''
						)}

						{this.props.screenName === 'fullView' &&
						this.props.allowSuggestEdit ? (
							<a className="header-item main-menu-icon fav-icon">
								<Editicon
									onClick={() =>
										this.props.toggleSuggestEdits()
									}
								/>
							</a>
						) : this.props.screenName === 'fullView' &&
						  this.props.allowSuggestEdit === false ? (
							''
						) : this.props.screenName === 'favourites' ||
						  this.props.screenName === 'projectShare' ? (
							<a
								className="header-item main-menu-icon fav-icon"
								onClick={() => this.props.onClick()}
							>
								<Closeicon />
							</a>
						) : this.props.screenName === 'fullView-favts' ? (
							''
						) : (
							<a
								className="header-item main-menu-icon fav-icon"
								onClick={() => this.props.toggleFavourites()}
							>
								<Favicon />
							</a>
						)}
					</div>
				)}
			</div>
		);
	}
}

export default Header;
